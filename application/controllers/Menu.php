<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	private $user;

	function __construct(){
		parent::__construct();
		$this->load->library('encryption');

		//Check if user is loggedIn ?
		$authkey = $this->session->userdata("login_code");
		if("shirazInn" != $this->encryption->decrypt($authkey)){
			redirect(base_url("admin"));
			exit;
		}

		$this->load->model("crud");
		$this->config->load('messages');
		$this->user = $this->session->userdata("user");
	}

	public function index(){
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->join('category', 'category.id = menu.cat_id');
		//$this->db->join('hall', 'hall.hid = menu.hall_id');
		$query = $this->db->get();
		$menu_list = $query->result_array();

		foreach ($menu_list as $key=>$menu) {
			$items = explode(",", $menu["items"]);
			$menu_list[$key]["item_details"] = $this->crud->in("id,title", "items_list", "id", $items);
		}

		$data["menu_list"] = $menu_list;
		$data["page"] = "admin/menu-list";
		$data["user"] = $this->user;
		$this->load->view("backend/index", $data);
	}

	public function items(){

		if($this->input->post("action")){
			$action = $this->input->post("action");
		}
		else{
			$action = $this->uri->segment(3);
			$id = $this->uri->segment(4);
		}

		switch ($action) {
			case md5("add"):
				$data = $this->input->post("item");
				if ($this->db->insert('items_list', $data)) {
					$this->session->set_flashdata('success', $this->config->item("ILM3"));
					redirect(base_url("menu/items"));
					exit;
				}
				break;

			case md5("update"):
				$data = $this->input->post("item");
				$id = $this->input->post("id");
				if ($this->db->update('items_list', $data, array('id' => $id))) {
					$this->session->set_flashdata('success', $this->config->item("ILM4"));
					redirect(base_url("menu/items"));
					exit;
				}
				break;

			case md5("edit"):
				$item = $this->crud->read_row("*", "items_list", array(
					"id" => $id
				));
				$data["item"] = $item;
				break;

			case md5("delete"):
				if($this->crud->delete("id", $id, "items_list")){
					$this->session->set_flashdata('success', $this->config->item("ILM2"));
					redirect(base_url('menu/items'));
					exit;
				}
				break;
			
			default:
				break;
		}

		$data["items_list"] = $this->crud->read("*", "items_list");
		$data["page"] = "admin/items";
		$data["user"] = $this->user;
		$this->load->view("backend/index", $data);
	}

}