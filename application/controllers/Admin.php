<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	private $user;

	function __construct(){
		parent::__construct();
		$this->load->model("crud");
		$this->load->library('encryption');
		$this->config->load('messages');
		$this->user = $this->session->userdata("user");
	}

	public function index(){
		$authkey = $this->session->userdata("login_code");
		if("shirazInn" == $this->encryption->decrypt($authkey)){
			switch ($this->user["type"]) {
				case 'admin':
					$data["page"] = "admin-dashboard";
					$data["user"] = $this->user;
					$this->load->view("backend/index", $data);
					break;
				
				default:
					break;
			}
		}
		else{
			$this->load->view("backend/login");
		}
	}

	public function logedIn(){
		$post = $this->input->post();
		if(!empty($post["username"]) && !empty($post["password"])){
			$user = $this->crud->read_row("*", "user", array(
				"email" => $post["username"],
				"password" => md5($post["password"]),
			));
		}
		else{
			$this->session->set_flashdata('error', $this->config->item("LM1"));
			$url = base_url("admin");
			redirect($url);
		}
		

		if(!empty($user)){
			$this->session->set_userdata("user", $user);
			$this->session->set_userdata("login_code", $this->encryption->encrypt("shirazInn"));
			$url = base_url("admin");
			redirect($url);
		}
		else{
			$this->session->set_flashdata('error', $this->config->item("LM2"));
			$url = base_url("admin");
			redirect($url);
		}

		

	}

	public function ajax_actions(){
	}

	
}
