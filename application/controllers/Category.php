<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	private $user;

	function __construct(){
		parent::__construct();
		$this->load->library('encryption');

		//Check if user is loggedIn ?
		$authkey = $this->session->userdata("login_code");
		if("shirazInn" != $this->encryption->decrypt($authkey)){
			redirect(base_url("admin"));
			exit;
		}

		$this->load->model("crud");
		$this->config->load('messages');
		$this->user = $this->session->userdata("user");
	}

	public function index($data=""){
		$data["category_list"] = $this->crud->read("*", "category");
		$data["page"] = "admin/category-list";
		$data["user"] = $this->user;
		$this->load->view("backend/index", $data);
	}

	public function add(){
		if($this->input->post("action") == md5("add")){
			$data = $this->input->post("category");
			$insert_id = $this->db->insert('category', $data);
			if($insert_id) {
				$this->session->set_flashdata('success', $this->config->item("CLM1"));
				redirect(base_url("category"));
				exit;
			}
		}
	}

	public function edit(){
		$action = $this->uri->segment(3);
		if($action == md5("edit")){
			$id = $this->uri->segment(4);
			$data["category"] = $this->crud->read_row("*", "category", array("id" => $id));
		}

		$this->index($data);
	}

	public function update(){
		if($this->input->post("action") == md5("update")){
			$data = $this->input->post("category");
			$id = $this->input->post("id");
			$updated = $this->db->update('category', $data, array('id' => $id));
			
			if($updated){
				$this->session->set_flashdata('success', $this->config->item("CLM2"));
				redirect(base_url("category"));
				exit;
			}
		}
	}

	public function delete(){
		$action = $this->uri->segment(3);
		if($action == md5("delete")){
			$id = $this->uri->segment(4);
			if($this->crud->delete("id", $id, "category")){
				$this->session->set_flashdata('success', $this->config->item("CLM4"));
				redirect(base_url('category'));
				exit;
			}
		}
	}

}
