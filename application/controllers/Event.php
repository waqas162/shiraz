<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("crud");
		$this->load->helper('string');
		$this->config->load("messages");
		date_default_timezone_set("Asia/Karachi");
	}

	public function index(){
		if($this->session->flashdata("success_message")){
			$items = array('eid','time','date','type','guests','hall_charges','male_guests','female_guests','halls','menu_id','price','extra_items');
			$this->session->unset_userdata($items);
		}

		$data["page"] = "events/list-events";
		$data["events"] = $this->crud->read("*", "event", array(
			'date >=' => date("o-m-d", strtotime("-6 months")),
			'date <=' => date("o-m-d", strtotime("+6 months")),
		));
		$data["session"] = $this->session->userdata();
		$this->load->view("index", $data);
	}

	public function menu(){
		if($this->input->post("hall")){
			//$event_id = "H".$this->input->post("hall")."D".date("jnY", strtotime($this->input->post("event_date"))).strtoupper(substr($this->input->post("event_time"), 0, 1));

			//If event type is seperate
			if($this->input->post("event_type") == "separate"){
				$this->session->set_userdata(array(
					"male_guests" => (int) $this->input->post("guests") - (int) $this->input->post("female_guests"),
					"female_guests" => $this->input->post("female_guests"),
				));
			}
			else{
				$this->session->unset_userdata(array("male_guests", "female_guests"));
			}

			$hall_list = array();
			if($this->input->post("hall") == "sah"){
				foreach ($this->input->post("arena") as $arena_hall) {
					array_push($hall_list, $arena_hall);
				}
			}
			else{
				array_push($hall_list, $this->input->post("hall"));
			}

			$this->session->set_userdata("halls", $hall_list);

			$event_id = "H".implode("H", $hall_list)."D".strtotime($this->input->post("event_date")).strtoupper(substr($this->input->post("event_time"), 0, 1));
			$this->session->set_userdata(array(
				"eid" => $event_id,
				"time" => $this->input->post("event_time"),
				"date" => date("o-m-d", strtotime($this->input->post("event_date"))),
				"type" => $this->input->post("event_type"),
				"guests" => $this->input->post("guests"),
				"hall_charges" => $this->input->post("hall_charges"),
			));
		}

		//Redirect if no session
		if(empty($this->session->userdata("eid"))){
			$url = base_url();
			redirect($url);
		}
		
		$output = array();
		//Load Categories
		$output = $this->crud->read("*", "category");
		foreach ($output as $cat_key=>$category) {
			//Load Menu list
			$output[$cat_key]["menu_list"] =  $this->crud->read("*", "menu", array("cat_id" => $category["id"]));
			foreach ($output[$cat_key]["menu_list"] as $menu_key => $menu) {
				$list = explode(",", $output[$cat_key]["menu_list"][$menu_key]["items"]);
				//$extra = explode(",", $output[$cat_key]["menu_list"][$menu_key]["extra"]);

				//Load items list 
				$output[$cat_key]["menu_list"][$menu_key]["items"] = $this->crud->in("*", "items_list", "id", $list);

				//load extra list
				//$output[$cat_key]["menu_list"][$menu_key]["extra"] = $this->crud->in("*", "extra_items", "id", $extra);
			}
		}
		
		$data["session"] = $this->session->userdata();
		$data["result"] = $output;
		$data["page"] = "events/menu";
		$this->load->view("index", $data);
	}

	public function extra(){
		if($this->input->post("menu_id")){
			$this->session->set_userdata(array(
				"menu_id" => $this->input->post("menu_id"),
				"price" => $this->input->post("price")
			));
		}
		
		//Redirect if no session
		if(empty($this->session->userdata("menu_id"))){
			$url = base_url();
			redirect($url);
		}

		$data["session"] = $this->session->userdata();
		//pre($data["session"]);
		$data["extra_items"] = $this->crud->read("*", "extra_items");
		$data["page"] = "events/extras";
		$this->load->view("index", $data);
	}

	public function ajax_actions(){
		$action = $this->input->post("action");
		switch ($action) {
			case 'check_event':
				$post = $this->input->post("event_data");
				
				/*$reserved_halls = $this->crud->read("*", "event", array(
					"date" => $post["date"],
					"time" => $post["event_time"],
					"hall_id" => $post["hall"],
					"status !=" => "lock"
				));*/

				$query = $this->db->query("SELECT * FROM `event` WHERE `date` = '".$post["date"]."' AND `time` = '".$post["event_time"]."' AND `status` != 'lock' AND (`hall_id` LIKE '%,".$post["hall"]."' ESCAPE '!' OR `hall_id` LIKE '".$post["hall"]."' ESCAPE '!' OR `hall_id` LIKE '%,".$post["hall"].",%' ESCAPE '!')");
				
				$reserved_halls = $query->result_array();

				if(empty($reserved_halls)){
					$hall = $this->crud->read_row("*", "hall", array(
						"hid" => $post["hall"]
					));

					if($post["guests"] >= $hall["min"] && $post["guests"] <= $hall["max"]){
						$data["success"] = 1;
					}
					elseif($post["guests"] < $hall["min"]){
						//$data["error"] = "No. of guests are not enough to select this hall";
						$diff = $hall["min"] - $post["guests"];
						$charges = $diff * $hall["per_head_charge"];
						$data["charges"] = "Hall Charges Rs. ".$charges;
						$data["charges"] .= '<input type="hidden" name="hall_charges" value="'.$charges.'" />';
					}
					elseif($post["guests"] > $hall["max"]){
						$data["error"] = $this->config->item("EM2");
					}


				}
				else{
					$data["error"] = $this->config->item("EM1");
				}

				echo json_encode($data);
				break;

			case 'check_arena_hall':
				$post = $this->input->post("event_data");

				$query = $this->db->query("SELECT * FROM `event` WHERE `date` = '".$post["date"]."' AND `time` = '".$post["event_time"]."' AND `status` != 'lock' AND (`hall_id` LIKE '%,".$post["hall"]."' ESCAPE '!' OR `hall_id` LIKE '".$post["hall"]."' ESCAPE '!' OR `hall_id` LIKE '%,".$post["hall"].",%' ESCAPE '!')");

				$reserved_halls = $query->result_array();


				if(!empty($reserved_halls)){
					$data["error"] = $this->config->item("EM1");
					echo json_encode($data);
					exit;
				}

				$hall = $this->crud->read_row("*", "hall", array(
					"hid" => $post["hall"]
				));

				if(!empty($post["previous"])){
					//Check, that any need to select further halls
					$prev_hall_list = $this->crud->in("*", "hall", "hid", $post["previous"]);
					//$data["query"] = $this->db->last_query();
					foreach ($prev_hall_list as $row) {
						if($post["guests"] < $row["min"]){
							$data["overflow"] = 1;
							$diff = $hall["min"] - $post["guests"];
							$charges = $diff * $hall["per_head_charge"];
							$data["charges"] = "Hall Charges Rs. ".$charges;
							$data["charges"] .= '<input type="hidden" name="hall_charges" value="'.$charges.'" />';
							echo json_encode($data);
							exit;
						}
					}


					($post["status"] == "false") ? array_push($post["previous"], $post["hall"]) : "";
					//$data["data"] = $post["previous"];
					$hall = $this->crud->calculate_hall_range(
						array("min", "max", "per_head_charge"),
						"hall",
						$post["previous"]
					);
				}

				if($post["guests"] >= $hall["min"] && $post["guests"] <= $hall["max"]){
					$data["success"] = 1;
				}
				elseif($post["guests"] < $hall["min"]){
					if($post["status"] == "true" && empty($post["previous"])){
						$charges = 0;
						$data["charges"] = "Hall Charges Rs. ".$charges;
					}
					else{
						$diff = $hall["min"] - $post["guests"];
						$charges = $diff * $hall["per_head_charge"];
						$data["charges"] = "Hall Charges Rs. ".$charges;
						$data["charges"] .= '<input type="hidden" name="hall_charges" value="'.$charges.'" />';
					}
					//$data["error"] = "No. of guests are not enough to select this hall";
					//if($post["status"] == "false" && empty($post["previous"])){
						
					/*}
					else{
						$charges = 0;
						$data["charges"] = "Hall Charges Rs. ".$charges;
					}*/
					

				}
				elseif($post["guests"] > $hall["max"]){
					$data["success"] = $this->config->item("EM3");
				}

				echo json_encode($data);

			break;

			case 'uncheck_arena_hall':
				$post = $this->input->post("event_data");
				$hall = $this->crud->read_row("*", "hall", array(
					"hid" => $post["hall"]
				));

				break;

			case 'validate_event':
				$post = $this->input->post("event_data");

				if(!empty($post["hall"])){
					$hall = $this->crud->calculate_hall_range(
						array("min", "max"),
						"hall",
						$post["hall"]
					);

					if($post["guests"] >= $hall["min"] && $post["guests"] <= $hall["max"]){
						$data["success"] = 1;
					}
					elseif($post["guests"] < $hall["min"]){
						//$data["error"] = "No. of guests are not enough to select this hall";
						$data["success"] = 1;
					}
					elseif($post["guests"] > $hall["max"]){
						$data["error"] = $this->config->item("VM2");
					}

				}
				else{
					$data["error"] = $this->config->item("VM1");
				}

				echo json_encode($data);

				break;
			
			default:
				break;
		}
	}

	
}
