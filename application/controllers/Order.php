<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("crud");
		$this->load->library('icalendar');
		$this->config->load("messages");
		date_default_timezone_set("Asia/Karachi");
	}

	public function index(){
		$extras = $this->input->post("extra_items");
		$no_extra = false;

		if (!empty($extras)) {
			$this->session->set_userdata(array(
				"extra_items" => $this->input->post("extra_items")
			));

			$data["extras"] = $this->crud->in("*", "extra_items", "id", $extras);
		}
		elseif($this->session->userdata("extra_items")){
			$data["extras"] = $this->crud->in("*", "extra_items", "id", $this->session->userdata("extra_items"));
			$no_extra = true;
		}
		else{
			$data["extras"] = "";
			$no_extra = true;
		}

		//Redirect if no session
		if($no_extra == true){
			$url = base_url();
			redirect($url);
		}
		
		$data["menu"] = $this->crud->read_row("*", "menu", array(
			"mid" => $this->session->userdata("menu_id")
		));
		
		$data["halls"] = $this->crud->in("*", "hall", "hid", $this->session->userdata("halls"));
		$data["order"] = $this->session->userdata();

		//Calculate Islamic Date
		$GDay = date("j", strtotime($this->session->userdata("date")));
		$GMonth = date("n", strtotime($this->session->userdata("date")));
		$GYear = date("Y", strtotime($this->session->userdata("date")));
		$data["acdate"] = $this->icalendar->GregorianToIslamic($GYear, $GMonth, $GDay);

		$data["session"] = $this->session->userdata();
		$data["page"] = "orders/place";
		$this->load->view("index", $data);
	}

	public function place(){
		if(!empty($this->input->post("checkout_total"))){
			$user = $this->input->post("user");
			$session = $this->session->userdata();

			//Check if user exists
			$user_data = $this->crud->read_row("*", "user", array(
				"contact" => $user["contact"]
			));

			//First insert to User table
			if(empty($user_data)){
				$insert_id = $this->db->insert("user", $user);
			}
			else{
				$insert_id = $user_data["id"];
			}

			//Insert to Event
			$event_id = $this->db->insert("event", array(
				"eid" => $session["eid"],
				"user_id" => $insert_id,
				"date" => $session["date"],
				"time" => $session["time"],
				"category" => "",
				"type" => $session["type"],
				"guests" => $session["guests"],
				"male_guests" => @$session["male_guests"],
				"female_guests" => @$session["female_guests"],
				"hall_id" => implode(",", $session["halls"]),
				"menu_id" => $session["menu_id"],
				"extras" => implode(",", $session["extra_items"]),
				"amount" => $this->input->post("checkout_total")
			));
			
			if($event_id){

				if(!empty($user["contact"])){
					$message = str_replace('[EID]', $session["eid"], $this->config->item("EM5"));
					$message = urlencode($message);
					sendText($user["contact"], $message);
				}
				
				$this->session->set_flashdata("success_message", $this->config->item("EM4"));
				$url = base_url();
				redirect($url);
			}
		}

		
		//pre($this->input->post());
		//pre($this->session->userdata());
	}
}