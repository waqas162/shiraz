<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function index(){

	}

	public function read($select="*", $table="", $where=array()){
		//$this->db->reset_query();
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->result_array();		
	}

	public function read_row($select="*", $table="", $where=array()){
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->row_array();		
	}

	public function in($select="*", $table="", $column="", $where=array()){
		$this->db->select($select);
		$this->db->where_in($column, $where);
		$query = $this->db->get($table);
		return $query->result_array();		
	}

	public function group_by($select="*", $table="", $by="", $where=array()){
		$this->db->select($select);
		$this->db->where($where);
		$this->db->group_by($by);
		$query = $this->db->get($table);
		return $query->result_array();		
	}

	/*public function check_available_halls($post){
		$this->db->select('*');
		$this->db->from('event');
		$this->db->join('hall', 'hall.hid = event.hall_id');
		$this->db->where(array(
			"event.date" => $post["date"],
			"event.time" => $post["event_time"],
			"event.hall_id" => $post["hall"]
		));
		$query = $this->db->get();
		return $query->result_array();
	}*/

	/*public function load_menu(){
		$this->db->select('*');
		$this->db->from('menu');
		$this->db->join('category', 'menu.cat_id = category.id');
		$query = $this->db->get();
		return $query->result_array();
	}*/

	public function calculate_hall_range($column_list=array(), $from="", $hall_list){
		foreach ($column_list as $column) {
			$this->db->select_sum($column);
		}
		$this->db->from($from);
		$this->db->where_in("hid", $hall_list);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function delete($col="", $val="", $table=""){
		if(!empty($col && $val)){
			$this->db->where($col, $val);
			$this->db->delete($table);
			return true;
		}

		return false;
	}

	
}

?>