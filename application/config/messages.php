<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Event Registration
|--------------------------------------------------------------------------
|
| Event Registration Messages
| 
*/
$config["EM1"] = 'Someone has already reserved this hall';
$config["EM2"] = 'Overflow. Try Shiraz Gathering or Arena two halls combination'; //Overflow
$config["EM3"] = 'Overflow. Try two or three halls combination'; //Overflow Arena Halls
$config["EM4"] = 'Event date reserved successfully';
$config["EM5"] = 'Event date has been reserved. Your track id is: [EID]';


/*
|--------------------------------------------------------------------------
| Event Validation
|--------------------------------------------------------------------------
|
| Event Validation Messages
| 
*/
$config["VM1"] = 'Select hall first';
$config["VM2"] = 'Overflow. Try another large hall or Arena two halls combination';


/*
|--------------------------------------------------------------------------
| Login Validation
|--------------------------------------------------------------------------
|
| Login Validation Messages
| 
*/
$config["LM1"] = 'Make sure that you enter all details';
$config["LM2"] = 'Invalid email or password';


/*
|--------------------------------------------------------------------------
| Items List
|--------------------------------------------------------------------------
|
| Items List Messages
| 
*/
$config["ILM1"] = 'Are you sure you want to delete this item ?';
$config["ILM2"] = 'Item deleted successfully';
$config["ILM3"] = 'Item added successfully';
$config["ILM4"] = 'Item updated successfully';


/*
|--------------------------------------------------------------------------
| Category List
|--------------------------------------------------------------------------
|
| Category List Messages
| 
*/
$config["CLM1"] = 'Category added successfully';
$config["CLM2"] = 'Category updated successfully';
$config["CLM3"] = 'Are you sure you want to delete this category ?';
$config["CLM4"] = 'Category deleted successfully';