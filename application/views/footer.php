</div> <!-- WRAPPER ends -->

<!-- ============ FOOTER ================== -->
<footer>
    <div class="container">
        <article class="social-btn-group">
            <ul class="social-btns clearfix">
                  <li><a class="youtube" href="javascript:void(0)"><i class="fa fa-youtube"></i></a></li>
                  <li><a class="vimeo" href="javascript:void(0)"><i class="fa fa-vimeo-square"></i></a></li>
                    <li><a class="twitter" href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                  <li><a class="facebook" href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
            </ul>
        </article>


        <article class="design">
            <a href="http://www.hiwaas.com">Designed &amp; Developed by Hiwaas</a>
        </article>

        <article class="copyright">
            <h5>2015 Hiwaas</h5>
        </article>

        <a class="logo" href="javascript:void(0)">
        <h1>AWESOME SPICES</h1>
      </a>
  </div>
</footer>

		</div> <!-- inside-body-wrapper ends -->
	</body>
</html>