<div class="main_content">
	<?php
		if($this->session->flashdata('success')){
			echo '<div class="alert alert-success alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("success").'</div>';
		}
		elseif($this->session->flashdata('error')){
			echo '<div class="alert alert-danger alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("error").'</div>';
		}
	?>
	<div class="row">
		<div class="col-sm-7 col-md-7">
			<h3 class="heading">Category List</h3>
			<table class="table table-striped table-bordered dTableR">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$categ_count = 1;
					foreach ($category_list as $row) {
					?>
					<tr>
						<td><?php echo $categ_count; ?></td>
						<td><?php echo $row["title"]; ?></td>
						<td><?php echo $row["desc"]; ?></td>
						<td>
							<div class="btn-group pull-right">
								<a class="btn btn-default btn-sm" href="<?php echo base_url("category/edit/".md5("edit")."/".$row["id"]); ?>"><i class="glyphicon glyphicon-pencil"></i></a>
								<a class="btn btn-default btn-sm" href="<?php echo base_url("category/delete/".md5("delete")."/".$row["id"]); ?>" onclick="return confirm('<?php echo $this->config->item("CLM3"); ?>');"><i class="glyphicon glyphicon-remove"></i></a>
							</div>
						</td>
					</tr>
					<?php
						$categ_count++;
					}

					//pre($item);
					?>
				</tbody>
			</table>
		</div>
		<div class="col-sm-5 col-md-5">
			<h3 class="heading">Add/Update Category</h3>
			<form class="form-horizontal" action="<?php echo (empty($category)) ? base_url("category/add") : base_url("category/update"); ?>" method="post">
				<div class="form-group">
					<label class="col-md-2 control-label">Title</label>
					<div class="col-md-10">
						<input type="text" class="form-control" placeholder="Categ Name/Title" name="category[title]" value="<?php echo (!empty($category["title"])) ? $category["title"] : ''; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Description</label>
					<div class="col-md-10">
						<textarea class="form-control" name="category[desc]"><?php echo (!empty($category["desc"])) ? $category["desc"] : ''; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
					<?php
						if(!empty($category)){
							?>
							<input type="hidden" name="id" value="<?php echo $category["id"]; ?>" />
							<input type="hidden" name="action" value="<?php echo md5("update"); ?>" />
							<button type="submit" class="btn btn-default">Update</button>
							<a href="<?php echo base_url("category"); ?>" class="btn btn-default">Cancel</a>
							<?php
						}
						else{
							?>
							<input type="hidden" name="action" value="<?php echo md5("add"); ?>" />
							<button type="submit" class="btn btn-default">Add</button>
							<?php
						}
					?>
					</div>
				</div>
			</form>
		</div>
	</div> <!-- End of row -->
</div> <!-- End of main content -->

<script type="text/javascript">
	$(document).ready(function(){
		var myTable = $('table').dataTable({
            "sDom": "<'row'<'col-sm-6'<'dt_actions'>l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "sPaginationType": "bootstrap",
            "aoColumns": [
				{ "bSortable": false },//ID
				{ "bSortable": true },//Title
				//{ "sType": "string" },//Ingredients
				{ "bSortable": false },//Ingredients
				{ "bSortable": false },//Actions
			]
        });
 
	});	
</script>