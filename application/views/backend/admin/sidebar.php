<div class="sidebar">
	<div class="sidebar_inner_scroll">
		<div class="sidebar_inner">
			<!-- <form action="search_page.html" class="input-group input-group-sm" method="post">
				<input autocomplete="off" name="query" class="search_query form-control input-sm" size="16" placeholder="Search..." type="text">
				<span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></span>
			</form> -->
			<div id="side_accordion" class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="#collapseCateg" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
							<i class="glyphicon glyphicon-folder-close"></i> Categories
						</a>
					</div>
					<div class="accordion-body collapse" id="collapseCateg">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="<?php echo base_url("category"); ?>">Categoies</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
							<i class="glyphicon glyphicon-folder-close"></i> Menu
						</a>
					</div>
					<div class="accordion-body collapse" id="collapseOne">
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li><a href="<?php echo base_url("menu/items"); ?>">Items</a></li>
								<li><a href="<?php echo base_url("menu"); ?>">Menu</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div class="push"></div>
		</div>
					   
		<!-- <div class="sidebar_info">
			<ul class="list-unstyled">
				<li>
					<span class="act act-warning">65</span>
					<strong>New comments</strong>
				</li>
				<li>
					<span class="act act-success">10</span>
					<strong>New articles</strong>
				</li>
				<li>
					<span class="act act-danger">85</span>
					<strong>New registrations</strong>
				</li>
			</ul>
		</div>  -->
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".accordion-toggle").click(function(){
			var iclass = $(this).find("i").attr("class");
			if(iclass == "glyphicon glyphicon-folder-close"){
				$(this).find("i").removeClass("glyphicon-folder-close").addClass("glyphicon-folder-open");
			}
			else if(iclass == "glyphicon glyphicon-folder-open"){
				$(this).find("i").removeClass("glyphicon-folder-open").addClass("glyphicon-folder-close");
			}
		});
	});
</script>