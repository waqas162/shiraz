<div class="main_content">
	<?php
		if($this->session->flashdata('success')){
			echo '<div class="alert alert-success alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("success").'</div>';
		}
		elseif($this->session->flashdata('error')){
			echo '<div class="alert alert-danger alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("error").'</div>';
		}
	?>
	<div class="row">
		<div class="col-sm-7 col-md-7">
			<h3 class="heading">Items List</h3>
			<table class="table table-striped table-bordered dTableR">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Ingredients</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$item_count = 1;
					foreach ($items_list as $row) {
					?>
					<tr>
						<td><?php echo $item_count; ?></td>
						<td><?php echo $row["title"]; ?></td>
						<td><?php echo $row["ingredients"]; ?></td>
						<td>
							<div class="btn-group pull-right">
								<a class="btn btn-default btn-sm" href="<?php echo base_url("menu/items/".md5("edit")."/".$row["id"]); ?>"><i class="glyphicon glyphicon-pencil"></i></a>
								<a class="btn btn-default btn-sm" href="<?php echo base_url("menu/items/".md5("delete")."/".$row["id"]); ?>" onclick="return confirm('<?php echo $this->config->item("ILM1"); ?>');"><i class="glyphicon glyphicon-remove"></i></a>
							</div>
						</td>
					</tr>
					<?php
						$item_count++;
					}

					//pre($item);
					?>
				</tbody>
			</table>
		</div>
		<div class="col-sm-5 col-md-5">
			<h3 class="heading">Add/Update List</h3>
			<form class="form-horizontal" action="<?php echo base_url("menu/items"); ?>" method="post">
				<div class="form-group">
					<label class="col-md-2 control-label">Title</label>
					<div class="col-md-10">
						<input type="text" class="form-control" placeholder="Item Name/Title" name="item[title]" value="<?php echo (!empty($item["title"])) ? $item["title"] : ''; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Ingredients</label>
					<div class="col-md-10">
						<textarea class="form-control" name="item[ingredients]"><?php echo (!empty($item["ingredients"])) ? $item["ingredients"] : ''; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
					<?php
						if(!empty($item)){
							?>
							<input type="hidden" name="id" value="<?php echo $item["id"]; ?>" />
							<input type="hidden" name="action" value="<?php echo md5("update"); ?>" />
							<button type="submit" class="btn btn-default">Update</button>
							<a href="<?php echo base_url("menu/items"); ?>" class="btn btn-default">Cancel</a>
							<?php
						}
						else{
							?>
							<input type="hidden" name="action" value="<?php echo md5("add"); ?>" />
							<button type="submit" class="btn btn-default">Add</button>
							<?php
						}
					?>
					</div>
				</div>
			</form>
		</div>
	</div> <!-- End of row -->
</div> <!-- End of main content -->

<script type="text/javascript">
	$(document).ready(function(){
		var myTable = $('table').dataTable({
            "sDom": "<'row'<'col-sm-6'<'dt_actions'>l><'col-sm-6'f>r>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "sPaginationType": "bootstrap",
            "aoColumns": [
				{ "bSortable": false },//ID
				{ "bSortable": true },//Title
				//{ "sType": "string" },//Ingredients
				{ "bSortable": false },//Ingredients
				{ "bSortable": false },//Actions
			]
        });
 
	});	
</script>