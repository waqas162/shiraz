<div class="main_content">
	<?php
		if($this->session->flashdata('success')){
			echo '<div class="alert alert-success alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("success").'</div>';
		}
		elseif($this->session->flashdata('error')){
			echo '<div class="alert alert-danger alert-dismissable"><a data-dismiss="alert" class="close">&times;</a>'.$this->session->flashdata("error").'</div>';
		}
	?>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<h3 class="heading">Menu List</h3>
			<table class="table table-striped table-bordered dTableR">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Items</th>
						<th>Category</th>
						<th>Price</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$count = 1;
					foreach ($menu_list as $menu) {
					?>
					<tr>
					<td><?php echo $count; ?></td>
					<td><?php echo $menu["name"]; ?></td>
					<td>
						<ul class="list_a">
						<?php
							foreach ($menu["item_details"] as $row) {
							?>
							<li><?php echo $row["title"]; ?></li>
							<!-- <span class="label label-shiraz"><?php echo $row["title"]; ?></span> -->
							<?php
							}
						?>
						</ul>
					</td>
					<td><?php echo $menu["title"]; ?></td>
					<td><?php echo $menu["price"]; ?></td>
					<td><?php echo $menu["status"]; ?></td>
					<td></td>
					</tr>
					<?php
					$count++;
					}
				?>
				</tbody>
			</table>
		</div> <!-- End of col-md -->
	</div> <!-- End of row -->
</div> <!-- End of main content -->