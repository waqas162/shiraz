<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Shiraz Admin Panel</title>
    
        <!-- Bootstrap framework -->
        <link rel="stylesheet" href="<?php echo base_url("bootstrap/css/bootstrap.min.css"); ?>" />
        
        <!-- jQuery UI theme -->
        <link rel="stylesheet" href="<?php echo base_url("lib/jquery-ui/css/Aristo/Aristo.css"); ?>" />
        
        <!-- jBreadcrumbs-->
        <!-- <link rel="stylesheet" href="<?php //echo base_url("lib/jBreadcrumbs/css/BreadCrumb.css"); ?>" /> -->
        
        <!-- tooltips-->
        <link rel="stylesheet" href="<?php echo base_url("lib/qtip2/jquery.qtip.min.css"); ?>" />
        
		<!-- colorbox -->
        <link rel="stylesheet" href="<?php echo base_url("lib/colorbox/colorbox.css"); ?>" />
        
        <!-- code prettify -->
        <link rel="stylesheet" href="<?php echo base_url("lib/google-code-prettify/prettify.css"); ?>" /> 
           
        <!-- sticky notifications -->
        <link rel="stylesheet" href="<?php echo base_url("lib/sticky/sticky.css"); ?>" /> 
           
        <!-- aditional icons -->
        <link rel="stylesheet" href="<?php echo base_url("img/splashy/splashy.css"); ?>" />
        
		<!-- flags -->
        <link rel="stylesheet" href="<?php echo base_url("img/flags/flags.css"); ?>" />
        	
        <!-- datatables -->
        <link rel="stylesheet" href="<?php echo base_url("lib/datatables/extras/TableTools/media/css/TableTools.css"); ?>">


        <!-- main styles -->
        <link rel="stylesheet" href="<?php echo base_url("css/style.css"); ?>" />
        
		<!-- theme color-->
        <link rel="stylesheet" href="<?php echo base_url("css/blue.css"); ?>" id="link_theme" />	
            
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
	
        <!-- favicon -->
        <link rel="shortcut icon" href="<?php echo base_url("favicon.ico"); ?>" />
		
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="<?php echo base_url("css/ie.css"); ?>" />
        <![endif]-->
        	
        <!--[if lt IE 9]>
			<script src="<?php echo base_url("js/ie/html5.js"); ?>"></script>
			<script src="<?php echo base_url("js/ie/respond.min.js"); ?>"></script>
			<script src="<?php echo base_url("lib/flot/excanvas.min.js"); ?>"></script>
        <![endif]-->


        <script src="<?php echo base_url("js/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("js/jquery-migrate.min.js"); ?>"></script>
        <script src="<?php echo base_url("lib/jquery-ui/jquery-ui-1.10.0.custom.min.js"); ?>"></script>

        <!-- touch events for jquery ui-->
        <script src="<?php echo base_url("js/forms/jquery.ui.touch-punch.min.js"); ?>"></script>

        <!-- easing plugin -->
        <script src="<?php echo base_url("js/jquery.easing.1.3.min.js"); ?>"></script>

        <!-- smart resize event -->
        <script src="<?php echo base_url("js/jquery.debouncedresize.min.js"); ?>"></script>

        <!-- js cookie plugin -->
        <script src="<?php echo base_url("js/jquery_cookie_min.js"); ?>"></script>

        <!-- main bootstrap js -->
        <script src="<?php echo base_url("bootstrap/js/bootstrap.min.js"); ?>"></script>

        <!-- bootstrap plugins -->
        <script src="<?php echo base_url("js/bootstrap.plugins.min.js"); ?>"></script>

        <!-- typeahead -->
        <script src="<?php echo base_url("lib/typeahead/typeahead.min.js"); ?>"></script>

        <!-- code prettifier -->
        <script src="<?php echo base_url("lib/google-code-prettify/prettify.min.js"); ?>"></script>

        <!-- sticky messages -->
        <script src="<?php echo base_url("lib/sticky/sticky.min.js"); ?>"></script>

        <!-- tooltips -->
        <script src="<?php echo base_url("lib/qtip2/jquery.qtip.min.js"); ?>"></script>

        <!-- lightbox -->
        <script src="<?php echo base_url("lib/colorbox/jquery.colorbox.min.js"); ?>"></script>

        <!-- jBreadcrumbs -->
        <!-- <script src="<?php //echo base_url("lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"); ?>"></script> -->

        <!-- hidden elements width/height -->
        <script src="<?php echo base_url("js/jquery.actual.min.js"); ?>"></script>

        <!-- custom scrollbar -->
        <script src="<?php echo base_url("lib/slimScroll/jquery.slimscroll.js"); ?>"></script>

        <!-- fix for ios orientation change -->
        <script src="<?php echo base_url("js/ios-orientationchange-fix.js"); ?>"></script>

        <!-- to top -->
        <script src="<?php echo base_url("lib/UItoTop/jquery.ui.totop.min.js"); ?>"></script>

        <!-- mobile nav -->
        <script src="<?php echo base_url("js/selectNav.js"); ?>"></script>

        <!-- datatable -->
        <script src="<?php echo base_url("lib/datatables/jquery.dataTables.min.js"); ?>"></script>
        <script src="<?php echo base_url("lib/datatables/extras/Scroller/media/js/dataTables.scroller.min.js"); ?>"></script>

        <!-- datatable table tools -->
        <script src="<?php echo base_url("lib/datatables/extras/TableTools/media/js/TableTools.min.js"); ?>"></script>
        <script src="<?php echo base_url("lib/datatables/extras/TableTools/media/js/ZeroClipboard.js"); ?>"></script>
        
        <!-- datatables bootstrap integration -->
        <script src="<?php echo base_url("lib/datatables/jquery.dataTables.bootstrap.min.js"); ?>"></script>

        <!-- common functions -->
        <script src="<?php echo base_url("js/gebo_common.js"); ?>"></script>


    </head>
    <body class="full_width">
		<div id="maincontainer" class="clearfix">
			<header>
				<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
					<div class="navbar-inner">
						<div class="container">
							<a class="brand pull-left" href="dashboard.html">Shiraz Admin <span class="sml_t"></span></a>
							
							<ul class="nav navbar-nav user_menu pull-right">
								<li class="divider-vertical hidden-sm hidden-xs"></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url("img/user_avatar.png"); ?>" alt="" class="user_avatar"><?php echo $user["name"]; ?> <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href=".html">My Profile</a></li>
										<li class="divider"></li>
										<li><a href="javascrip:void(0)">Log Out</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</header>

            <div id="contentwrapper">