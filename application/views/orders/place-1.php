<div class="menu-page">

  <section class="banner">
        <!-- <article class="banner-img">
          <img src="_assets/images/banner-img.jpg" height="460" width="1920" alt="">
        </article> -->

        <div class="container">
          <article class="banner-caption">
            <h5><span></span> &nbsp;&nbsp;Book an event with us&nbsp;&nbsp; <span></span></h5>
            <h2>events</h2>
          </article>
        </div>
      </section>

      <?php 
       /* pre($this->session->userdata()); 
        pre($menu);
        pre($extras);*/
      ?>
      <section class="main-content">
        <div class="container">

          <div class="head clearfix">
            <div class="page-name">
              <h3>Place Order</h3>
            </div>
            <div class="order-date-details">
              <h5>Your order ID is: <span id="orderid"><?php echo @$order["eid"]; ?></span> </h5>
              <h6 id="order-date">Order Date: <?php echo @date("l, j M o", strtotime($order["date"])); ?></h6>
            </div>
          </div>
          <div class="sub-head clearfix">
            <div class="captn">
              <h6 style="font-size: 14px;">Your selected menu and extra items are listed below</h6>
            </div>
            <!-- <div class="no-of-dishes">
              <h6>You have selected <span class="selected-dishes-no-pop">0</span> dishes.</h6>
              <div>
                <img src="_assets/images/chef-hat-icon-grey.png" height="40" width="41" alt="">  
                <span class="selected-dishes-no-pop">0</span>
              </div>
            </div> -->
          </div>
          <div class="items-wrapper">
            <!-- ============= HEADER ================== -->
            <div class="order-pg-items tophead clearfix">
              <div class="item-details-price clearfix">
                <div class="order-item-details clearfix">
                  <div class="figcap">
                    <h4>Menu/Item Name</h4>
                  </div>
                  <div class="figcap rate">
                    <h4>Price</h4>
                  </div>
                </div>
                <div class="order-item-price clearfix">
                  <h4>Total</h2>
                  <h4>Guests</h2>
                </div>
              </div>
              <div class="replace-cancel clearfix">
                  <div class="replace-cancel-btn-wrapper clearfix">Type</div>
                </div>
            </div>

            <div id="ordered-items">
              <div class="order-pg-items clearfix">
                <div class="item-details-price clearfix">
                  <div class="order-item-details clearfix">
                    <div class="figcap"><?php echo $menu["name"] ?></div>
                    <div class="figcap rate">Rs <?php echo $menu["price"]; ?></div>
                  </div>
                  <div class="order-item-price clearfix">
                    <h2 class="item-total">Rs <?php echo (int) $menu["price"] * (int) $order["guests"];  ?></h2>
                    <!-- <input type="text" value="Vanilla Crème Brûlée" class="hidden-field hid_rcp">
                    <input type="text" value="10.90" class="hidden-field hid">
                    <input type="text" value="10.90" class="hidden-field tot" name="val[]"> -->
                    <!-- <select class="item-select sel" name="">
                      <option value="1">For 1 person</option>
                      <option value="2">For 2 person</option>
                      <option value="3">For 3 person</option>
                      <option value="4">For 4 person</option>
                    </select> -->
                    <div class="figcap guests"><?php echo $order["guests"] ?></div>
                  </div>
                </div>
                <div class="replace-cancel clearfix">
                  <div class="replace-cancel-btn-wrapper clearfix" style="top:6px;">Menu</div>
                </div>
              </div>
              <?php
                $extra_total = 0;
                foreach ($extras as $item) {
                  $total = (int) $item["price"] * (int) $order["guests"];
                  $extra_total = $extra_total + $total;
                ?>
                <div class="order-pg-items clearfix">
                  <div class="item-details-price clearfix">
                    <div class="order-item-details clearfix">
                      <div class="figcap"><?php echo $item["title"] ?></div>
                      <div class="figcap rate">Rs <?php echo $item["price"]; ?></div>
                    </div>
                    <div class="order-item-price clearfix">
                      <h2 class="item-total">Rs <?php echo $total;  ?></h2>
                      <div class="figcap guests"><?php echo $order["guests"] ?></div>
                    </div>
                  </div>
                  <div class="replace-cancel clearfix">
                    <div class="replace-cancel-btn-wrapper clearfix" style="top:6px;">Extra item</div>
                  </div>
                </div>
                <?php
                }
              ?>
            </div>
            
          </div>

          <div class="order-value clearfix">
            <div class="total-value-details">
              <h4>Your total value is</h4>
              <h5>After adding all tax</h5>
              <!-- <div class="discount-promo">
                <h5>Use discount promo</h5>
                <span>qwe2554er</span>
              </div> -->
            </div>
            <div id="total_amount" class="total-value">
              <input type="text" class="hidden-field" value="" />
              <h2>Rs <?php echo $extra_total + ((int) $menu["price"] * (int) $order["guests"]) ?></h2>
            </div>
          </div>

          <form id="order-form" method="post" action="">

            <div id="dishes"></div>
            <div class="info-address">
              <h3>your info and address</h3>
              <div class="clearfix outer-wrapper">
                <div class="input-wrapper">
                  <input type="text" id="orderName" name="orderName" placeholder="Your name">
                </div>
                <div class="input-wrapper">
                  <input type="text" id="orderEmail" name="orderEmail" placeholder="Email Id">
                </div>
              </div>
              <div class="clearfix outer-wrapper">
                <div class="input-wrapper">
                  <input type="text" id="orderAddress" name="orderAddress" placeholder="Write your full address">
                </div>
                <div class="clearfix half">
                  <div class="input-wrapper">
                    <input type="text" id="orderPincode" name="orderPincode" placeholder="Zip code">
                  </div>
                  <div class="input-wrapper">
                    <input type="text" id="orderPhone" name="orderPhone" placeholder="Phone no">
                  </div>
                </div>
              </div>
            </div>
            <div class="final-order clearfix">
              <button class="button green-btn" onclick="return false;">order now</button>
              <div class="order-msg form-message" >
                <div><div class="loader">Loading...</div></div>
              </div>
            </div>
          </form>


        </div>
      </section>
</div>