<?php
  /*pre($halls);
  pre($extras);
  pre($menu);
  pre($order);*/

  $subtotal = 0;
  $total = 0;

  $hall_charges = (!empty($order["hall_charges"])) ? $order["hall_charges"] : 0;

?>
<style type="text/css">
  select:focus, input:focus, textarea:focus{
    border: 1px solid #66afe9 !important;
    outline: medium none !important;
  }
</style>
<div class="main_content" style="background: none; width: 90%; margin: 50px auto auto; padding-bottom: 80px;">

<form method="post" action="<?php echo base_url("order/place"); ?>">
  
  <div class="col-md-12 col-sm-12 clearfix" style="text-align:center; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top:60px;">
    <ul class="stepy-titles clearfix">
      <li data-url="<?php echo base_url(); ?>">
        <div>Date</div>
        <span>Select Event Date</span>
        <span class="stepNb">1</span>
      </li>
      <li <?php echo (!empty($session["eid"])) ? 'data-url="'.base_url("event/menu").'"' : ''; ?>>
        <div>Menu</div>
        <span>Select Event Menu</span>
        <span class="stepNb">2</span>
      </li>
      <li <?php echo (!empty($session["menu_id"])) ? 'data-url="'.base_url("event/extra").'"' : ''; ?>>
        <div>Extras</div>
        <span>Select menu extra items</span>
        <span class="stepNb">3</span>
      </li>
      <li class="error-image current-step">
        <div>Order</div>
        <span>Reserve Event Date</span>
        <span class="stepNb">4</span>
      </li>
    </ul>
  </div>
  <div style="clear:both;">&nbsp;</div>

  <div class="row" style="margin-top:40px;">
    <div class="col-sm-8 col-md-8">
      <h1 class="invoice_heading">Order ID #<?php echo $order["eid"]; ?></h1>
    </div>
    <div class="col-sm-4 col-md-4">
      <p class="sepH_a"><span class="sepV_b text-muted">Invoice No.</span><strong><?php echo $order["eid"]; ?></strong></p>
      <p class="sepH_a"><span class="sepV_b text-muted">Invoice Date</span><strong><?php echo date("F j, Y"); ?></strong></p>
      <!-- <p><span class="sepV_b text-muted">Payment Due</span><strong>24/11/2012</strong></p> -->
    </div>
  </div>



  <div class="vcard">
    <ul style="margin:0px; padding:0px;">
      <li class="v-heading">Order Details</li>
      <li>
        <span class="item-key">Date/Time</span>
        <div class="vcard-item">
        <?php 
          echo date("j M Y", strtotime($order["date"]));
          echo ", ".ucfirst($order["time"]);
        ?>
        <span style="float: right; direction: rtl;">
        <?php
          echo $acdate["day"]." ".$acdate["monthname"]." ".$acdate["year"]
        ?>
        </span>
      </li>
      <li>
        <span class="item-key">Hall</span>
        <div class="vcard-item">
          <ul class="list_a">
          <?php
            foreach ($halls as $hall) {
              echo '<li>'.$hall["name"].'</li>';
            }
          ?>
          </ul>
        </div>
      </li>
      <li>
        <span class="item-key">Type</span>
        <div class="vcard-item"><?php echo ucfirst($order["type"]); ?> Event</div>
      </li>
      <li>
        <span class="item-key">Guests</span>
        <div class="vcard-item">
          <?php
            if($order["type"] == "separate"){
            ?>
            <ul>
              <li>Total: <?php echo $order["guests"]; ?></li>
              <li>Male: <?php echo $order["male_guests"]; ?></li>
              <li>Female: <?php echo $order["female_guests"]; ?></li>
            </ul>
            <?php
            }
            else{
              echo $order["guests"];
            }
          ?>
          
        </div>
      </li>
      <li>
        <span class="item-key">Menu</span>
        <div class="vcard-item"><?php echo $menu["name"]; ?></div>
      </li>
      <li class="v-heading">User Info</li>
      <li>
        <span class="item-key">Full name <span class="f_req">*</span></span>
        <div class="vcard-item">
          <div class="col-sm-4 col-md-4" data-message="Please enter your full name">
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-user"></span></span>
              <input class="form-control" type="text" name="user[name]" value="" />
            </div>
            <span class="help-block"></span>  
          </div>
        </div>
      </li>
      <li>
        <span class="item-key">Email <span class="f_req">*</span></span>
        <div class="vcard-item">
          <div class="col-sm-4 col-md-4" data-message="Please enter your email">
            <div class="input-group">
              <span class="input-group-addon"><span class="icon-envelope-alt"></span></span>
              <input class="form-control" type="text" name="user[email]" value="" />
            </div>
            <span class="help-block"></span>  
          </div>
        </div>
      </li>
      <li>
        <span class="item-key">Contact <span class="f_req">*</span></span>
        <div class="vcard-item">
          <div class="col-sm-4 col-md-4" data-message="Please enter your contact number">
            <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
              <input class="form-control" type="text" name="user[contact]" />
            </div>
            <span class="help-block"></span>  
          </div>
        </div>
      </li>
      <li>
        <span class="item-key">Address <span class="f_req">*</span></span>
        <div class="vcard-item">
          <div class="col-sm-4 col-md-4" data-message="Please enter your address">
            <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-home"></span></span>
              <textarea class="form-control" name="user[address]"></textarea>
            </div>
            <span class="help-block"></span>  
          </div>
        </div>
      </li>
    </ul>
  </div>


<div class="row" style="margin-top:50px;">
  <div class="col-sm-12 col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Item</th>
          <th>Rate</th>
          <th>Count</th>
          <th class="invoice_tar">Subtotal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $menu["name"]; ?></td>
          <td><?php echo $menu["price"]; ?></td>
          <td><?php echo $order["guests"]; ?></td>
          <td class="invoice_tar">Rs.<?php echo $subtotal = $menu["price"] * $order["guests"]; ?></td>
        </tr>
        <tr>
      <?php
        if(!empty($extras)){
          foreach ($extras as $item) {
            $guest_count = ($item["price_type"] == "repeated") ? $order["guests"] : 1;
            $subtotal = $subtotal + ($item["price"] * $guest_count);
          ?>
          <tr>
            <td><?php echo $item["title"]; ?></td>
            <td><?php echo $item["price"]; ?></td>
            <td><?php echo $guest_count; ?></td>
            <td class="invoice_tar">Rs.<?php echo $item["price"] * $guest_count; ?></td>
          </tr>
          <?php
          }
        }
      ?>
        <tr>
          <td colspan="3">&nbsp;</td>
          <td class="invoice_tar">
            <p class="sepH_a"><span class="sepV_b text-muted">Subtotal</span>Rs.<?php echo number_format($subtotal); ?></p>
            <p class="sepH_a"><span class="sepV_b text-muted">Service Charges </span>Rs.
            <?php
              $service_charges = 12000;
              $total = $subtotal + $service_charges;
              echo number_format($service_charges);
            ?>
            </p>
            <p class="sepH_a"><span class="sepV_b text-muted">Hall Charges </span>Rs.
            <?php 
              $total = $total + $hall_charges;
              echo number_format($hall_charges); 
            ?>
            </p>
            <p class="sepH_a"><span class="sepV_b text-muted">Tax </span>0</p>
            <!-- <p class="sepH_a"><span class="sepV_b text-muted">Discount</span>0</p> -->
            <p class="sepH_a text-success"><strong><span class="sepV_b">Total</span>Rs.<?php echo number_format($total); ?></strong></p>
            <input type="hidden" name="checkout_total" value="<?php echo $total; ?>" />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <button style="float:right" type="submit" class="button red-btn" id="place_order">Place Order</button>
  </div>
</div>

</form>

<div class="row">
  <div class="col-sm-12 col-md-12">
    <p class="sepH_a"><span class="label label-info">Notes</span></p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget consectetur ante. Nunc vitae nunc vel elit imperdiet bibendum.</p>
  </div>
</div>                
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $(".vcard-item").on("keydown", 'input[name="user[contact]"]', function(e){
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13/*, 110, 190*/]) !== -1 ||
          (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });

    $("#place_order").click(function(e){
      $(".vcard").find(":input").each(function(){
        //Reset
        $('.col-md-4').removeClass("f_error");
        $(".help-block").html("");

        var elem = $(this);
        var value = elem.val();
        var col_md = $(this).parents(".col-md-4");
        var message = col_md.attr("data-message");

        if(!value || value == ""){
          $(col_md).addClass("f_error").find(".help-block").html(message);
          elem.focus();
          e.preventDefault();
          return false;
        }

        if(elem.attr("name") == 'user[email]' && !isValidEmailAddress(value)){
          $(col_md).addClass("f_error").find(".help-block").html('Please enter a valid email address');
          elem.focus();
          e.preventDefault();
          return false;
        }

      });

      

    });

  });

  function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
</script>