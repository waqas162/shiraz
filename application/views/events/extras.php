<style type="text/css">
	.food-items{
		float: left;
		width: 70%;
	}

	.popular-events-widget .widget_heading {
	  border-left: 2px solid #dd4f45;
	  border-bottom: 1px solid #dadada;
	  margin: 0;
	  padding: 20px 15px;
	  text-transform: uppercase;
	}

	.popular-events-widget .popular-events-list{
		display: block;
		height: 500px;
		overflow: auto;
	}

	.popular-events-widget .popular-events-list .figcaption{
		padding: 0px;
		padding: 10px 10px 5px;
    	float: none;
    	width: auto;
    	text-align: left;
	}

	.popular-events-widget .popular-events-list .figcaption i{
		background-color: #dd4f45;
	    color: #fff;
	    font-size: 12px;
	    padding: 4px;
	    position: absolute;
	    right: 0;
	    top: 0;
	    cursor: pointer;
	    display: block;
	}

	.popular-events-widget .popular-events-list .figcaption:hover i{
		display: block;
	}

	.popular-events-widget .popular-events-list .figcaption h4{
		font-size: 14px;
	}

	.popular-events-widget .popular-events-list .figcaption h2{
		width: auto;
		font-size: 15px;
		float: none;
		margin-bottom: 0px;
		margin-top: 10px;
	}

	.popular-events-widget .popular-events-list .figcaption h2 span{
		font-size: 13px;
		vertical-align: top;
	}

	.food-gallery .food-item-wrapper{
		width: 49%;
	}
</style>
<div class="food-gallery">

	<section class="banner">
		<!-- <article class="banner-img">
			<img src="_assets/images/food-solution-bg.jpg" alt="">
		</article> -->

		<div class="container">
			<article class="banner-caption">
				<h5><span></span> &nbsp;&nbsp;Book an event with us&nbsp;&nbsp; <span></span></h5>
				<h2>events</h2>
			</article>
		</div>
	</section>


	<section class="main-content">
		<div class="container">
			
			<div class="col-md-12 col-sm-12 clearfix" style="text-align:center; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top:40px;">
				<ul class="stepy-titles clearfix">
					<li data-url="<?php echo base_url(); ?>">
						<div>Date</div>
						<span>Select Event Date</span>
						<span class="stepNb">1</span>
					</li>
					<li <?php echo (!empty($session["eid"])) ? 'data-url="'.base_url("event/menu").'"' : ''; ?>>
						<div>Menu</div>
						<span>Select Event Menu</span>
						<span class="stepNb">2</span>
					</li>
					<li class="error-image current-step" <?php echo (!empty($session["menu_id"])) ? 'data-url="'.base_url("event/extra").'"' : ''; ?>>
						<div>Extras</div>
						<span>Select menu extra items</span>
						<span class="stepNb">3</span>
					</li>
					<li <?php echo (!empty($session["extra_items"])) ? 'data-url="'.base_url("order").'"' : ''; ?>>
						<div>Order</div>
						<span>Reserve Event Date</span>
						<span class="stepNb">4</span>
					</li>
				</ul>
			</div>
			<div style="clear:both;">&nbsp;</div>

			<form method="post" action="<?php echo base_url("order"); ?>">
				<div class="food-options-form">
					<div class="caption">
	                  <h3>select extra items</h3>
	                  <h6>Complete your menu from our wide range of items.</h6>
	                </div>
	                <!-- <div class="options-button clearfix">
	                	<div class="form-btn-area clearfix">
	                    <div class="no-of-dishes">
	                      <h6>You have selected <span class="selected-dishes-no">0</span> items.</h6>
	                      <div>
	                        <img src="<?php echo base_url("_assets/images/currency-pkr.png"); ?>" height="40" width="41" alt="">
	                        <span class="menu_price">0</span>
	                      </div>
	                    </div>
	                    <div class="button-holder">
	                      <a class="button red-btn" href="javascript:void(0)" onclick="javascript: next_to_order(this);">Next</a>
	                    </div>
	                  </div>
	                </div> -->
				</div>

				<ul class="food-type-list clearfix" id="food-type-list">
					<li><a href="#food-type-list" class="selected" data-filter=".food">Food</a></li>
					<li><a href="#food-type-list" data-filter=".music">Music</a></li>
					<li><a href="#food-type-list" data-filter=".dj">DJ</a></li>
					<li><a href="#food-type-list" data-filter=".stage">Stage</a></li>
					<li><a href="#food-type-list" data-filter=".drinks">drinks</a></li>
					<li><a href="#food-type-list" data-filter=".extras">Extras</a></li>
				</ul>

				<article class="content-wrapper clearfix food-items" id="food-items">
				<?php
					foreach ($extra_items as $item) {
						$checked = (!empty($session["extra_items"]) && in_array($item["id"], $session["extra_items"])) ? 'checked="checked"' : "";
					?>
					<div class="food-item-wrapper <?php echo $item["category"]; ?>">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="<?php echo base_url("uploads/".$item["image"]); ?>" alt="extra item image">
							</div>

							<div class="figcaption clearfix">
								<h3 style="height:55px;"><?php echo $item["title"] ?></h3>
								<div class="clearfix">
									<div class="make-switch" id="switch_<?php echo $item["id"]; ?>" data-on="info" data-off="danger" data-label-icon="icon-white" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
								    	<input type="checkbox" value="<?php echo $item["id"] ?>" name="extra_items[]" data-price="<?php echo $item["price"] ?>" <?php echo $checked; ?> />
									</div>
								</div>
								<h2><span>Rs</span><?php echo $item["price"] ?></h2>
							</div>
						</div>
					</div>
					<?php
					}
				?>
				</article>

				<article class="widget-sectn" style="margin: 0px; padding: 40px 0;">
					

					<article class="popular-events-widget">
						<h4 class="widget_heading">Selected Items <span class="selected-dishes-no" style="float: right;">0</span></h4>

						<!-- <h4 class="widget_heading menu_price">Price <span class="item_price" style="text-transform: capitalize; font-size: 20px; float:right;">Rs.0</span></h4> -->

						<h4 class="widget_heading menu_price">Menu <span class="menu_price" style="text-transform: capitalize; font-size: 20px; float:right;">Rs.<?php echo $session["price"]; ?></span></h4>

						<h4 class="widget_heading menu_price">Total <span class="total_price_top" style="text-transform: capitalize; font-size: 20px; float:right;"></span></h4>

						<div class="widget_heading"><a class="button red-btn" href="javascript:void(0)" onclick="javascript: next_to_order(this);">Next</a></div>
						<ul class="popular-events-list">
						<?php
							if(!empty($session["extra_items"])){

								$total = $session["price"];
								$count = 0;

								foreach ($extra_items as $item) {
									if(in_array($item["id"], $session["extra_items"])){
										$total = $total + $item["price"];
										$count++;
									?>
									<li class="switch_<?php echo $item["id"]; ?>">
										<div class="figcaption">
											<i class="fa fa-times"></i>
											<h4><?php echo $item["title"]; ?></h4>
											<h2>Rs <?php echo $item["price"]; ?></h2>
										</div>
		                            </li>
									<?php
									}
								}
							}
						?>
						</ul>
                    </article>




				</article>

				<div class="clearfix"></div>

				<div class="complete-search clearfix" style="margin-bottom:120px;">
	                <div class="left-sectn" style="float:left; width:auto;">
	                  <h3>completed your menu</h3>
	                  <h6>Now one more step to complete your order</h6>
	                </div>
	                <div class="right-sectn clearfix" style="float:right; width:auto;">
	                  <div class="no-of-dishes">
	                    <h6>You have selected <span class="selected-dishes-no">0</span> items.</h6>
	                    <div>
	                      <img src="<?php echo base_url("_assets/images/currency-pkr.png"); ?>" height="40" width="41" alt=""> 
	                      <span class="total_price_bottom">0</span>
	                    </div>
	                  </div>
	                  <!-- NO-OF-DISHES ends -->
	                  <div class="button-holder">
	                    <a class="button red-btn" href="javascript:void(0)" onclick="javascript: next_to_order(this);">Next</a>
	                  </div><!-- BUTTON-HOLDER ends -->
	                </div><!-- RIGHT-SECTN ends -->
	            </div>
			</form>
		</div> <!-- CONTAINER ends -->
	</section> <!-- MAIN CONTENT ends -->

</div>

<script type="text/javascript">
	$(document).ready(function(){
		var $container = jQuery('#food-items');

		$container.isotope({
			itemSelector : '.food-item-wrapper',
			filter: ".food",
  			animationOptions: {
			     duration: 750,
			     easing: 'linear',
			     queue: false
			   }
		});

		$("#food-type-list").find('li.selected').trigger("click");
	      
		$('.food-type-list li a').on('click',  function(e) {
			e.preventDefault();
		  	var filterValue = $(this).attr('data-filter');
		  	$container.isotope({ filter: filterValue });
		});


		<?php
		if(!empty($session["extra_items"])){
		?>
			$(".selected-dishes-no").html(<?php echo $count; ?>);
			$(".total_price_top").html("Rs."+<?php echo $total; ?>);
			$(".total_price_bottom").html(<?php echo $total; ?>);
			var dishes = <?php echo $count; ?>;
			var total = <?php echo $total; ?>;
		<?php
		}
		else{
		?>
			var dishes = 0;
			var total = <?php echo $session["price"]; ?>;
		<?php
		}
		?>
		
		
		$('.make-switch').on('switch-change', function(event, state) {

			var id = $(this).attr("id");
			var figcaption = $(this).parents(".figcaption");
			var title = figcaption.find("h3").html();
			var price = parseInt($(this).find('input[type="checkbox"]').attr("data-price"));

			if(state.value == true){
				dishes = dishes + 1;
				total = total + price;
				$(".popular-events-list").append('<li class="'+id+'"><div class="figcaption"><i class="fa fa-times"></i><h4>'+title+'</h4><h2>Rs.'+price+'</h2></div></li>');
			}
			else{
				dishes = dishes - 1;
				total = total - price;
				$(".popular-events-list").find("."+id).remove();
				//price = price - parseInt($(this).find('input[type="checkbox"]').attr("data-price"));
			}

			$(".selected-dishes-no").html(dishes);
			$(".total_price_top").html("Rs."+total);
			$(".total_price_bottom").html(total);
total
		});

		$(".popular-events-list").on("click", ".fa-times", function(){
			var id = $(this).parents("li").attr("class");
			$("#"+id).find('input[type="checkbox"]').trigger("click");
		})


	});

	function next_to_order(element){
		var form = get_form(element);
		form.submit();
	}
</script>