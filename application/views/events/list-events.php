<style type="text/css">
	.msgbox{height: 33px;}
	.loader{font-size: 5px;};
	.msgbox p{margin: 0px;}
	.event_loader{text-align: center;}
</style>
<div class="our-events">

			<section class="banner">
				<!-- <article class="banner-img">
					<img src="_assets/images/banner-img.jpg" height="460" width="1920" alt="">
				</article> -->

				<div class="container">
					<article class="banner-caption">
						<h5><span></span> &nbsp;&nbsp;Book an event with us&nbsp;&nbsp; <span></span></h5>
						<h2>events</h2>
					</article>
				</div>
			</section>


			<section class="main-content">
				<div class="container">

					<div class="col-md-12 col-sm-12 clearfix" style="text-align:center; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top:40px;">
						<ul class="stepy-titles clearfix">
							<li class="error-image current-step">
								<div>Date</div>
								<span>Select Event Date</span>
								<span class="stepNb">1</span>
							</li>
							<li <?php echo (!empty($session["eid"])) ? 'data-url="'.base_url("event/menu").'"' : ''; ?>>
								<div>Menu</div>
								<span>Select Event Menu</span>
								<span class="stepNb">2</span>
							</li>
							<li <?php echo (!empty($session["menu_id"])) ? 'data-url="'.base_url("event/extra").'"' : ''; ?>>
								<div>Extras</div>
								<span>Select menu extra items</span>
								<span class="stepNb">3</span>
							</li>
							<li <?php echo (!empty($session["extra_items"])) ? 'data-url="'.base_url("order").'"' : ''; ?>>
								<div>Order</div>
								<span>Reserve Event Date</span>
								<span class="stepNb">4</span>
							</li>
						</ul>
					</div>
					<div style="clear:both;">&nbsp;</div>
					
					<article class="content-wrapper clearfix">
						<?php
							if($this->session->flashdata("success_message")){
								echo '<div class="alert alert-success alert-dismissable"><a class="close" data-dismiss="alert" onclick="dismiss_lert(this)">&times</a>'.$this->session->flashdata("success_message").'</div>';
							}
						?>
						<h3>events calendar</h3>

						<div class="col-sm-12 col-md-12"><div id="event_cal"></div></div>


					</article> <!-- CONTENT-WRAPPER ends -->
				</div> <!-- CONTAINER ends -->
			</section> <!-- MAIN CONTENT ends -->



	<!-- <section class="map-search">
		<div class="container">
			<article class="city-search clearfix">
				<div class="search-caption">
					<h4>Track</h4>
					<h4>Your Event</h4>
				</div>

				<div class="search-form">
					<form>
						<input type="text" placeholder="Enter your pincode">
						<button>FIND NOW</button>
					</form>
				</div>
			</article>	
		</div>

		<div id="map-canvas" ></div>
	</section> -->
</div> <!-- EVENT ends -->


<!-- +++++++++++++++++++++ Event Popup +++++++++++++++++++++++++++++++++++ -->
<div class="overlay">
	<div class="modal right order-page" style="overflow:auto;">
		<i class="fa fa-times"></i>
		<div class="head clearfix">
			<div class="page-name" style="width:100%;">
				<h3>Event Details</h3>
			</div>
        </div>
        <form method="post" action="<?php echo base_url("event/menu"); ?>">
	        <div class="sub-head clearfix">
				<select name="event_time" data-message="Select event time">
					<option value="">Event Time</option>
					<option value="lunch">Lunch</option>
					<option value="dinner">Dinner</option>
				</select>
				<h6>Guests</h6>
				<div class="input-wrapper">
					<input type="text" placeholder="Number of guests" class="icons" name="guests" style="cursor: auto;" data-message="Enter total no of guests">
				</div>
				<h6>Event Type</h6>
				<div class="input-wrapper">
					<select name="event_type" id="event_type">
						<option value="mix">Mix</option>
						<option value="separate">Separate</option>
					</select>
				</div>
				<div class="female_guests"></div>
				
				<!-- <div class="input-wrapper clearfix" style="margin-top: 20px;">
		        	<button class="red-btn button" type="button" style="float: right; padding: 10px;" id="search_halls">Search</button>
		        </div> -->
	        </div>
        	<div class="items-wrapper">
	        	<div class="reservation">
	        		<h4>Select Halls</h4>
	        		<div class="available-msgbox msgbox">
	        			<p>Hall Charges Rs.0</p>
					</div>
					<div class="checkbox-container" id="hall_cont">
						<div class="arena">
							<div>
								<input type="radio" class="" id="hall_arena" name="hall" value="sah" />
								<label class="hall_arena" for="hall_arena" style="cursor:pointer;"><span></span> Arena</label>
							</div>
							<div class="sub_halls" style="margin-left: 30px; display:none;">
								<div>
									<input type="checkbox" class="clrd" id="hall_1" name="arena[]" value="1" />
									<label class="arena_hall" for="hall_1" style="cursor:pointer;"><span></span> Arena Ross Hall</label>
								</div>
								<div>
									<input type="checkbox" class="clrd" id="hall_2" name="arena[]" value="2" />
									<label class="arena_hall" for="hall_2" style="cursor:pointer;"><span></span> Arena Jasmine Hall</label>
								</div>
								<div>
									<input type="checkbox" class="clrd" id="hall_3" name="arena[]" value="3" />
									<label class="arena_hall" for="hall_3" style="cursor:pointer;"><span></span> Arena Daisy Hall</label>
								</div>
							</div>
						</div>
						<div>
							<input type="radio" class="clrd" id="hall_4" name="hall" value="4" />
							<label class="main_hall" for="hall_4" style="cursor:pointer;"><span></span> Shiraz Inn</label>
						</div>
						<div>
							<input type="radio" class="clrd" id="hall_5" name="hall" value="6" />
							<label class="main_hall" for="hall_5" style="cursor:pointer;"><span></span> Shiraz Gathering</label>
						</div>
						<input type="hidden" name="event_date" value="" />
						<div style="margin-top:20px;" class="clearfix">
							<a href="javascript:void(0)" class="button red-btn" onclick="next_to_menu(this);" style="float: right; clear: both; padding: 10px;">Next</a>
						</div>
					</div>
				</div>
	        </div>
	        <!-- <div class="items-wrapper">
	        	<div class="reservation">
	        		<h4>Hall Charges</h4>
					<div class="checkbox-container">
						<div>
			        		<input type="radio" class="clrd" id="" name="" value="" />
			        		<label for="" style="cursor:pointer;"><span></span>200-250 Rs. 70,000/= +Tax</label>
						</div>
					</div>
				</div>
	        </div> -->
        </form>
        
        
	</div>
</div>

<script type="text/javascript">
	
	var error = 0;
	var message = null;

	$(document).ready(function(){

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		var calendar = $('#event_cal').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: ''
			},
			eventLimit: true,
			aspectRatio: 2,
			selectable: true,
			selectHelper: true,
			dayRender: function (date, cell) {
				var today = new Date(y,m,d);
				var cell_date = new Date(date.format());
				if(cell_date < today){
					cell.css({
						"background-color": "#f9f9f9",
						"textColor": "red"
					});
				}
		       
		    },
			dayClick: function(date, jsEvent, view) {
				var cell = formatDate(date.format());
				var curr_date = new Date(y,m,d);
				var cell_date = new Date(date.format());

				if(cell_date >= curr_date){
					// Reset popup
					//$("#hall_cont").html("<p><i>Select event time to suggest available halls</i></p>");
					//$('#event_time').val("");

					
					$(".modal.right .page-name h3").text(cell).attr("data-date", date.format());
					$('input[name="event_date"]').val(date.format());
					$(".overlay").addClass("disp");
					var modalHeight = $(window).height();
					$('.inside-body-wrapper').css("max-height",modalHeight);
					$('.overlay').css("height",modalHeight);
					$('.modal').css({
						display: "block",
						height: modalHeight
					});
					$('.modal').removeClass('animated slideOutRight');
					$('.modal').addClass('animated slideInRight');
				}

				
				
		    },
			editable: false,
			theme: false,
			events: [
			<?php
				$events_list = "";

				foreach ($events as $event) {

					$format = date('o', strtotime($event["date"])) . "," . (date('n', strtotime($event["date"]))-1) ."," . date('d', strtotime($event["date"]));

					switch ($event["date"]){

						case ($event["date"] < date("o-m-d")):
							if($event["time"] == "lunch"){
								$events_list .= "{
									title: 'Lunch',
									start: new Date(".$format.',11,0'."),
									end: new Date(".$format.",15,0),
						            color: '#aedb97',
						            textColor: '#3c763d',
						            allDay: false
								},";
							}
							else{
								$events_list .= "{
									title: 'Dinner',
									start: new Date(".$format.',18,0'."),
									end: new Date(".$format.",23,0),
						            color: '#aedb97',
						            textColor: '#3c763d',
						            allDay: false
								},";
							}
							break;
						case (date("o-m-d")):
							if($event["time"] == "lunch"){
								$events_list .= "{
									title: 'Lunch',
									start: new Date(".$format.',11,0'."),
									end: new Date(".$format.",15,0),
						            color: '#d9edf7',
						            textColor: '#31708f',
						            allDay: false
								},";
							}
							else{
								$events_list .= "{
									title: 'Dinner',
									start: new Date(".$format.',18,0'."),
									end: new Date(".$format.",23,0),
						            color: '#d9edf7',
						            textColor: '#31708f',
						            allDay: false
								},";
							}
							break;
						case ($event["date"] > date("o-m-d")):
							if($event["time"] == "lunch"){
								$events_list .= "{
									title: 'Lunch',
									start: new Date(".$format.',11,0'."),
									end: new Date(".$format.",15,0),
						            color: '#f2dede',
						            textColor: '#994442',
						            allDay: false
								},";
							}
							else{
								$events_list .= "{
									title: 'Dinner',
									start: new Date(".$format.',18,0'."),
									end: new Date(".$format.",23,0),
						            color: '#f2dede',
						            textColor: '#994442',
						            allDay: false
								},";
							}
							break;
						default:
							break;
					}
				}

				echo $events_list;
			?>
			],
			eventColor: '#bcdeee'
		});
	
		$(".sub-head").on("keydown", 'input[name="male_guests"], input[name="female_guests"]', function(e){
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13/*, 110, 190*/]) !== -1 ||
			    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
			    (e.keyCode >= 35 && e.keyCode <= 40)) {
				console.log(e.keyCode);
			         return;
			}

			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			    e.preventDefault();
			}
	    });

		
		// Calculate Female guests
		$("#event_type").change(function(){
			var eType = $(this).val();
			if(eType == "separate"){
				$(".female_guests").html('<h6>Female Guests</h6><div class="input-wrapper"><input type="text" placeholder="Female guests" class="icons" name="female_guests" style="cursor: auto;" data-message="Enter total no of female guests" /></div>');
			}
			else{
				$(".female_guests").html('');
			}
		});

		//Apply Tooltip on main and sub Halls
		$(".main_hall, .arena_hall").parent().tooltipster({
    		theme: 'tooltipster-light',
		 	position: 'left',
		 	delay: '0',
    	});


		//Show/Hide and Reset sub Halls
    	$('input[name="hall"]:radio').change(function(){
    		var div = $(this).parent();
    		var input = $(this);

    		if(input.val() == "sah"){
    			$(".sub_halls").stop().show();
    		}
    		else{
    			$(".sub_halls").stop().hide();
    			$(".sub_halls").find(":input").prop("checked", false);
    			$(".arena_hall").parent().tooltipster("content", null);
    		}
    	});

    	// Reset All checkboxes and radio button on search filter change
    	$(".sub-head").find("select").change(function(){
    		$("#hall_cont").find(".clrd").prop("checked", false);
    		$(".main_hall, .arena_hall").parent().tooltipster("content", null);
    		error = 1;
    		message = "Select hall first";
    	});

    	$(".sub-head").find('input[type="text"]').keydown(function(){
    		$("#hall_cont").find(".clrd").prop("checked", false);
    		$(".main_hall, .arena_hall").parent().tooltipster("content", null);
    		error = 1;
    		message = "Select hall first";
    	});

    	//Sub Halls available for events ?
    	$(".sub_halls").find("label.arena_hall").click(function(e){
    		e.preventDefault();
    		var label = $(this);
			var div = label.parent();
			var input = div.find(":input");
			var status = input.prop("checked");

			(status == true) ? input.prop("checked", false) : "";

			//Reset
			$(".arena_hall").parent().tooltipster("content", null);
			$(".msgbox").html('<p class="loader"></p>');


			var obj = {};
			obj["date"] = $(".page-name h3").attr("data-date");
			error = 0;
			var field_val = "";
			message = "";

			$(".sub-head").find(":input").each(function(){
				if(this.type != "button"){
					var field_val = $(this).val();
					if(field_val && field_val != ""){
						obj[this.name] = $(this).val();
					}
					else{
						message = $(this).attr("data-message");
						error = 1;
						alert(message);
						return false;
					}
					
				}
			});

			//obj["hall"] = (status == false) ? input.val() : "";
			obj["hall"] = input.val();
			obj["status"] = status;
			var previous = new Array();

			div.parents(".arena").find('input[type="checkbox"]:checked').each(function(){
				previous.push($(this).val());
			});

			obj["previous"] = previous;


			if(error == 0){
				$.ajax({
		            type : "POST",
		            url : base_url('event/ajax_actions'),
		            data : {
		            	action: "check_arena_hall",
		            	event_data: obj,
		           	},
		           	dataType: "json",
		            beforeSend : function(){
		            },
		            success : function(data){
		            	if(data.success){
		            		(status == false) ? input.prop("checked", true) : "";
		            		if(data.success != 1){
		            			div.tooltipster("content", data.success);
		            			error = 1;
		            			message = data.success;
		            		}
		            		div.tooltipster('show');
		            		$(".msgbox").html('<p>Hall Charges Rs.0</p>');
		            	}

		            	if(data.charges && data.overflow){
		            		//div.tooltipster("content", data.overflow);
		            		//div.tooltipster('show');

		            		$(".sub_halls input:checkbox").each(function(){
		            			$(this).prop("checked", false);
		            		});
		            		(status == false) ? input.prop("checked", true) : "";
		            		input.trigger("change");
		            		$(".msgbox").html("<p>"+data.charges+"</p>");
		            	}
		            	else if(data.charges){
		            		(status == false) ? input.prop("checked", true) : "";
		            		input.trigger("change");
		            		$(".msgbox").html("<p>"+data.charges+"</p>");
		            	}

		            	if(data.error){
		            		$("#hall_cont").find(":checkbox").each(function(){
		            			$(this).prop("checked", false);
		            		});
		            		div.tooltipster("content", data.error);
		            		div.tooltipster('show');
		            		error = 1;
		            		message = data.error;
		            	}
		            }
		        });
			}

    	});


		// Main Halls available for events ?
		$("#hall_cont").find("label.main_hall").click(function(e){
			e.preventDefault();
			var label = $(this);
			var div = label.parent();
			var input = div.find(":input");

			//Reset
			$(".main_hall").parent().tooltipster("content", null);
			$(".msgbox").html('<p class="loader"></p>');

			var obj = {};
			obj["date"] = $(".page-name h3").attr("data-date");
			error = 0;
			var field_val = "";
			message = "";

			$(".sub-head").find(":input").each(function(){
				if(this.type != "button"){
					var field_val = $(this).val();
					if(field_val && field_val != ""){
						obj[this.name] = $(this).val();
					}
					else{
						message = $(this).attr("data-message");
						error = 1;
						alert(message);
						return false;
					}
					
				}
			});

			obj["hall"] = input.val();

			if(error == 0){
				$.ajax({
		            type : "POST",
		            url : base_url('event/ajax_actions'),
		            data : {
		            	action: "check_event",
		            	event_data: obj,
		           	},
		           	dataType: "json",
		            beforeSend : function(){
		            },
		            success : function(data){
		            	if(data.success){
		            		input.prop("checked", true);
		            		input.trigger("change");
		            		//div.tooltipster("content", "Available");
		            		div.tooltipster('show');
		            		$(".msgbox").html('<p>Hall Charges Rs.0</p>');
		            	}

		            	if(data.charges){
		            		input.prop("checked", true);
		            		input.trigger("change");
		            		$(".msgbox").html("<p>"+data.charges+"</p>");
		            	}

		            	if(data.error){
		            		$("#hall_cont").find(":radio, :checkbox").each(function(){
		            			$(this).prop("checked", false);
		            		});
		            		div.tooltipster("content", data.error);
		            		div.tooltipster('show');
		            		error = 1;
		            		message = data.error;
		            		$(".msgbox").html('<p>Hall Charges Rs.0</p>');
		            	}
		            }
		        });
			}

		});


	});

	function next_to_menu(element){
		var obj = {};
		var hall = new Array;
		obj["date"] = $(".page-name h3").attr("data-date");
		error = 0;
		var field_val = "";

		$(".sub-head").find(":input").each(function(){
			if(this.type != "button"){
				var field_val = $(this).val();
				if(field_val && field_val != ""){
					obj[this.name] = $(this).val();
				}
				else{
					message = $(this).attr("data-message");
					error = 1;
					alert(message);
					return false;
				}
				
			}
		});

		$("#hall_cont").find(":input:checked").each(function(){
			if(this.type != "button" && $.isNumeric($(this).val())){
				hall.push($(this).val());
			}
		});

		obj["hall"] = hall;

		if(error == 0){
			$.ajax({
	            type : "POST",
	            url : base_url('event/ajax_actions'),
	            data : {
	            	action: "validate_event",
	            	event_data: obj,
	           	},
	           	dataType: "json",
	            beforeSend : function(){
	            },
	            success : function(data){
	            	if(data.success){
	            		var form = get_form(element);
						form.submit();
	            	}

	            	if(data.error){
	            		alert(data.error);
	            	}
	            }
	        });
		}
		/*var elem = jQuery(element);
		var container = elem.parents("#hall_cont");

		var hall_selected = container.find('input[type="radio"]:checked').val();
		if(hall_selected && hall_selected != ""){
			console.log(hall_selected);
			var form = get_form(element);
			form.submit();
		}*/
		
	}

	function dismiss_lert(elem){
		var elem = $(elem);
		elem.parent(".alert-dismissable").hide(500);
	}

</script>