<style type="text/css">
  .brkfstSlider li.own, .lnchSlider li.own{
    float: left;
    list-style: outside none none;
    position: relative;
    /*width: 370px;*/
    width: 280px;
  }

  .search-menu-items .price-add-select a{
    float: none;
  }
</style>
<div class="menu-page">
  <section class="banner">
    <div class="good-food">
      <div class="container">
        <article class="banner-caption">
          <h5><span></span> &nbsp;&nbsp;Book an event with us&nbsp;&nbsp; <span></span></h5>
            <h2>events</h2>
        </article>
      </div>
    </div>
  </section>
  <section class="main-content">
    <div class="container">

      <div class="col-md-12 col-sm-12 clearfix" style="text-align:center; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; margin-top:40px;">
        <ul class="stepy-titles clearfix">
          <li data-url="<?php echo base_url(); ?>">
            <div>Date</div>
            <span>Select Event Date</span>
            <span class="stepNb">1</span>
          </li>
          <li class="error-image current-step">
            <div>Menu</div>
            <span>Select Event Menu</span>
            <span class="stepNb">2</span>
          </li>
          <li <?php echo (!empty($session["menu_id"])) ? 'data-url="'.base_url("event/extra").'"' : ''; ?>>
            <div>Extras</div>
            <span>Select menu extra items</span>
            <span class="stepNb">3</span>
          </li>
          <li <?php echo (!empty($session["extra_items"])) ? 'data-url="'.base_url("order").'"' : ''; ?>>
            <div>Order</div>
            <span>Reserved Event Date</span>
            <span class="stepNb">4</span>
          </li>
        </ul>
      </div>
      <div style="clear:both;">&nbsp;</div>
      
      <div class="caption food-options-form">
        <h3>select your Menu</h3>
        <h6><!-- A special way to select your food from our wide rnage of recipe. --></h6>
      </div>
      <form method="post" action="<?php echo base_url("event/extra"); ?>">
      <?php
        foreach ($result as $category) {
        ?>
          <article class="search-menu-list brkfast show">
            <div class="head">
              <img src="<?php echo base_url("_assets/images/food-menu-icon.png"); ?>" alt="">
              <h2><?php echo $category["title"] ?></h2>
              <h6><?php echo $category["desc"] ?></h6>
            </div>
            <div class="menu-items-wrapper" >
              <ul class="brkfstSlider clearfix">
              <?php
                foreach ($category["menu_list"] as $menu) {
                ?>
                  <li class="own animated fadeInUp" data-name="<?php echo $menu["mid"] ?>" data-price="<?php echo $menu["price"] ?>">
                    <div class="search-menu-items clearfix clearfix">
                      <!-- <figure>
                        <img src="<?php echo base_url("uploads/".$menu["image"]); ?>" alt="">
                      </figure> -->
                      <div class="figcaption clearfix">
                        <div>
                          <h4 style="font-weight: bold;"><?php echo $menu["name"] ?></h4>
                          <p></p>
                          <div class="item">
                            <h6>Items</h6>
                            <ul>
                            <?php
                              foreach ($menu["items"] as $item) {
                                echo '<li>'.$item["title"].'</li>';
                              }

                              /*if(!empty($menu["extra"])){
                                foreach ($menu["extra"] as $extra) {
                                  echo '<li><small><i>'.$extra["title"].'('.$extra["price"].' Rs Additional)</i></li></small>';
                                }
                              }*/
                              
                            ?>
                            </ul>
                          </div>
                        </div>
                        <div class="price-add-select clearfix">
                        <?php
                          if(!empty($session["menu_id"]) && $session["menu_id"] == $menu["mid"]){
                          ?>
                          <a class="button white-btn" href="javascript:void(0)">
                            <span class="desk">add to list</span>
                            <span class="mob"><i class="fa fa-check"></i></span>
                          </a>
                          <a class="button green-btn clicked" href="javascript:void(0)" onclick="get_form(this).submit(); return false" style="cursor: pointer;">
                            <span class="desk">Next</span>
                            <span class="mob"><i class="fa fa-check"></i></span>
                          </a>
                          <a class="button red-btn clicked" href="javascript:void(0)">X</a>
                          <?php
                          }
                          else{
                          ?>
                          <a class="button white-btn clicked" href="javascript:void(0)">
                            <span class="desk">add to list</span>
                            <span class="mob"><i class="fa fa-check"></i></span>
                          </a>
                          <a class="button green-btn" href="javascript:void(0)" onclick="get_form(this).submit(); return false" style="cursor: pointer;">
                            <span class="desk">Next</span>
                            <span class="mob"><i class="fa fa-check"></i></span>
                          </a>
                          <a class="button red-btn" href="javascript:void(0)">X</a>
                          <?php
                          }
                        ?>
                          <h4>Rs. <?php echo $menu["price"] ?>/= + Tax</h4>
                        </div>
                      </div>
                    </div>
                  </li>
                <?php
                }
              ?>
              </ul>
            </div>
          </article>
        <?php
        }
      ?>
        <input type="hidden" name="price" value="<?php echo (!empty($session["price"])) ? $session["price"] : ""; ?>" />
        <input type="hidden" name="menu_id" value="<?php echo (!empty($session["menu_id"])) ? $session["menu_id"] : ""; ?>" />
      </form>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    var item_list = new Array();
    $('.menu-items-wrapper div.item').each(function(){
      item_list.push($(this).height());
    });

    $(".menu-items-wrapper div.item").height(Math.max.apply(Math, item_list));
    //Math.max.apply(Math, item_list)
    //console.log(item_list);
  });
</script>
