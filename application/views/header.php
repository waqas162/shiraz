<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=Edge"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Shiraz</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url("dist/font-awesome.css"); ?>">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url("dist/jquery.bxslider.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("dist/animate.css"); ?>" media="screen" />

    <!-- switch buttons -->
    <link rel="stylesheet" href="<?php echo base_url("lib/bootstrap-switch/stylesheets/bootstrap-switch.css"); ?>" />

    <link rel="stylesheet" href="<?php echo base_url("style-admin.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("blue.css"); ?>">

    <link rel="stylesheet" href="<?php echo base_url("style.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("dist/desktop.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("dist/tab.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("dist/mobile.css"); ?>">

    <!-- calendar -->
    <link rel="stylesheet" href="<?php echo base_url("lib/fullcalendar/fullcalendar.css"); ?>" />

    <!-- Tooltipster -->
    <link rel="stylesheet" href="<?php echo base_url("lib/tooltipster/css/tooltipster.css"); ?>" /> 
    <link rel="stylesheet" href="<?php echo base_url("lib/tooltipster/css/themes/tooltipster-shadow.css"); ?>" />
    <link rel="stylesheet" href="<?php echo base_url("lib/tooltipster/css/themes/tooltipster-light.css"); ?>" />  

     <!-- Steps -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("lib/stepy/css/jquery.stepy.css"); ?>" /> 

    <!--[if IE]>
        <script src="<?php echo base_url("_assets/js/html5shiv.js"); ?>"></script>
        <link rel="stylesheet" href="<?php echo base_url("dist/ie9.css"); ?>">
    <![endif]-->

    <script src="<?php echo base_url("_assets/js/modernizr.custom.50095.js"); ?>"></script>
    <script src="<?php echo base_url("_assets/js/respond.js"); ?>"></script>
    
    <script src="<?php echo base_url("_assets/js/jquery-2.1.1.min.js"); ?>"></script>

    <script src="<?php echo base_url("_assets/js/jquery.validate.js"); ?>"></script>
    <script src="<?php echo base_url("_assets/js/jquery.fittext.js"); ?>"></script>
    <script src="<?php echo base_url("_assets/js/imgLiquid-min.js"); ?>"></script>
    <script src="<?php echo base_url("_assets/js/waypoints.min.js"); ?>"></script>
    <script src="<?php echo base_url("_assets/js/data.js"); ?>"></script>

    <!-- Calendar -->
    <script src="<?php echo base_url("lib/fullcalendar/extra/moment.min.js"); ?>"></script>
    <script src="<?php echo base_url("lib/fullcalendar/fullcalendar.min.js"); ?>"></script>

    <!-- Isotops -->
    <script src="<?php echo base_url("_assets/js/jquery.isotope.js"); ?>"></script>

    <!-- google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

    <!-- bxSlider Javascript file -->
    <script src="<?php echo base_url("_assets/js/jquery.bxslider.min.js"); ?>"></script>
    <script type="text/javascript">
        function base_url(str){
            return "<?php echo base_url(); ?>" + str;
        }    
    </script>

    <!-- switch buttons -->
    <script src="<?php echo base_url("lib/bootstrap-switch/js/bootstrap-switch.min.js"); ?>"></script>

    <!-- Tooltipster -->
    <script src="<?php echo base_url("lib/tooltipster/js/jquery.tooltipster.min.js"); ?>"></script>

    <script src="<?php echo base_url("_assets/js/myCustom.js"); ?>"></script>

  </head>

  <body>
    <div class="inside-body-wrapper">
    <header class="header-type2">
        <div class="container clearfix">
			<nav id="navigation-menu" class="nav-menu navbar-collapse" role="navigation">
                <div id="nav-div" class="clearfix">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target=".nav-menu">
                            <span class="sr-only">Toggle navigation</span> 
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <nav id="navigation-list" class="row navbar-collapse"  role="navigation">
                        <ul class="nav navbar-nav">
                            <!-- <li><a class="" href="javascript:void(0)">our events</a></li> -->
                            <!--<li class=""><a href="javascript:void(0)">PAGES<i class="fa fa-angle-right" ></i></a>
                                <ul>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="about-us.html">about us</a></li>
                                    <li><a href="contact-us.html">contact us</a></li>
                                    <li><a href="fav-dish.html">favourite dish</a></li>
                                    <li><a href="food-gallery.html">food gallery</a></li>
                                    <li><a href="know-chef.html">about chef</a></li>
                                    <li><a class="active" href="our-events.html">our events</a></li>
                                    <li><a href="our-gallery.html">gallery</a></li>
                                    <li><a href="social.html">social</a></li>
                                    <li><a href="testimonials.html">testimonials</a></li>
                                    <li><a href="reservation.html">reservation</a></li>
                                </ul>
                            </li>-->
                        </ul>
                    </nav>
                </div>
            </nav>
           	<img class="logo-shadow" src="<?php echo base_url("_assets/images/logo-shadow2.png"); ?>" alt="">
          	<div class="outer-ring">
              <div class="inner-ring">
                <a class="logo" href="javascript:void(0)">
                  <h1>Shiraz</h1>
                </a>
              </div>
            </div>
          
          	<div class="social-btns-group">
            	<a class="mobile-social-btn" href="javascript:void(0)"><i class="fa fa-share"></i></a>
                <ul class="social-btns clearfix">
                    <li><a class="youtube" href="javascript:void(0)"><i class="fa fa-youtube"></i></a></li>
                    <li><a class="vimeo" href="javascript:void(0)"><i class="fa fa-vimeo-square"></i></a></li>
                    <li><a class="twitter" href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="facebook" href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                </ul>          
            </div>
        </div>
    </header>
	<div class="wrapper">