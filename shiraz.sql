-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2015 at 12:53 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shiraz`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `desc` longtext
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `desc`) VALUES
(1, 'Desi', 'Pakistani Food'),
(2, 'Afghani', 'Afghani Food');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `eid` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `registration_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time` enum('lunch','dinner') NOT NULL DEFAULT 'lunch',
  `category` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `guests` int(11) NOT NULL,
  `male_guests` int(11) DEFAULT NULL,
  `female_guests` int(11) DEFAULT NULL,
  `hall_id` varchar(255) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `extras` varchar(255) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `status` enum('pending','lock') NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`eid`, `user_id`, `date`, `registration_date`, `time`, `category`, `type`, `guests`, `male_guests`, `female_guests`, `hall_id`, `menu_id`, `extras`, `amount`, `status`) VALUES
('1', '1', '2015-08-01', '2015-10-07 11:57:50', 'lunch', '', '', 0, NULL, NULL, '1', 0, '', 0, ''),
('2', '1', '2015-08-05', '2015-10-07 11:57:50', 'dinner', '', '', 0, NULL, NULL, '3', 0, '', 0, ''),
('3', '1', '2015-08-26', '2015-10-07 11:57:50', 'lunch', '', '', 0, NULL, NULL, '5', 0, '', 0, 'pending'),
('4', '1', '2015-08-26', '2015-10-07 11:57:50', 'dinner', '', '', 0, NULL, NULL, '5', 0, '', 0, 'pending'),
('5', '1', '2015-08-26', '2015-10-07 11:57:50', 'lunch', '', '', 0, NULL, NULL, '2', 0, '', 0, 'pending'),
('6', '1', '2015-09-13', '2015-10-07 11:57:50', 'lunch', '', '', 0, NULL, NULL, '1', 0, '', 0, 'pending'),
('7', '1', '2015-09-11', '2015-10-07 11:57:50', 'dinner', '', '', 0, NULL, NULL, '1', 0, '', 0, 'pending'),
('8', '1', '2015-09-11', '2015-10-07 11:57:50', 'lunch', '', '', 0, NULL, NULL, '2', 0, '', 0, 'pending'),
('H13D1445713200D', '1', '2015-10-25', '2015-10-08 16:44:57', 'dinner', '', 'mix', 550, NULL, NULL, '1,3', 10, NULL, 644500, 'pending'),
('H1D1446058800D', '1', '2015-10-29', '2015-10-28 16:01:41', 'dinner', '', 'mix', 200, NULL, NULL, '1', 1, '2', 244000, 'pending'),
('H1D1446058800L', '1', '2015-10-29', '2015-10-28 16:27:00', 'lunch', '', 'separate', 300, 260, 40, '1', 6, '4', 318000, 'pending'),
('H1H2H3D1444676400D', '1', '2015-10-13', '2015-10-08 16:59:10', 'dinner', '', 'separate', 700, 500, 200, '1,2,3', 4, '6', 1240000, 'pending'),
('H2D1444676400L', '1', '2015-10-13', '2015-10-12 16:54:17', 'lunch', '', 'separate', 150, 100, 50, '2', 4, '2', 325000, 'pending'),
('H3D1446231600D', '1', '2015-10-31', '2015-10-28 16:11:18', 'dinner', '', 'mix', 250, NULL, NULL, '3', 3, '1', 347000, 'pending'),
('H4D1444330800D', '2', '2015-10-09', '2015-10-08 16:55:56', 'dinner', '', 'mix', 120, NULL, NULL, '4', 6, NULL, 128400, 'pending'),
('H4D1444935600D', '1', '2015-10-16', '2015-10-08 14:04:10', 'dinner', '', 'mix', 100, NULL, NULL, '4', 5, '2', 253000, 'pending'),
('H4D1445972400L', '1', '2015-10-28', '2015-10-08 12:28:03', 'lunch', '', 'mix', 300, NULL, NULL, '4', 6, '5,6', 331000, 'pending'),
('H4D1446058800D', '1', '2015-10-29', '2015-10-08 12:11:02', 'dinner', '', 'mix', 150, NULL, NULL, '4', 6, '5', 166500, 'pending'),
('H4D1446145200L', '1', '2015-10-30', '2015-10-28 16:07:04', 'lunch', '', 'mix', 150, NULL, NULL, '4', 1, '2', 186000, 'pending'),
('H4D1446231600L', '8', '2015-10-31', '2015-10-08 16:41:53', 'lunch', '', 'mix', 20, NULL, NULL, '4', 6, NULL, 46400, 'pending'),
('H4D1447700400D', '1', '2015-11-17', '2015-10-08 14:15:09', 'dinner', '', 'mix', 50, NULL, NULL, '4', 9, '4', 67000, 'pending'),
('H6D1445281200L', '1', '2015-10-20', '2015-10-08 13:58:19', 'lunch', '', 'separate', 600, 400, 200, '6', 5, '4', 1440000, 'pending'),
('HsahD1444244400D', '1', '2015-10-08', '2015-10-08 14:08:05', 'dinner', '', 'separate', 350, 300, 50, '1,2', 3, '4', 560500, 'pending'),
('HsahD1446145200L', '1', '2015-10-30', '2015-10-07 16:00:05', 'lunch', '', 'separate', 350, 300, 50, '3', 6, '4,5', 390000, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `extra_items`
--

CREATE TABLE IF NOT EXISTS `extra_items` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `ingredients` longtext,
  `price` int(11) NOT NULL,
  `price_type` enum('single','repeated') NOT NULL DEFAULT 'repeated',
  `image` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extra_items`
--

INSERT INTO `extra_items` (`id`, `title`, `ingredients`, `price`, `price_type`, `image`, `category`) VALUES
(1, 'Fried Fish / Crispy Fried Fish ', NULL, 80, 'repeated', 'appetizers_01.jpg', 'food'),
(2, 'Kulfi', NULL, 80, 'repeated', 'Image-Indian-Kulfi-Ice-cream-resized_03.jpg', 'food'),
(3, 'Ice Cream', NULL, 90, 'repeated', 'ice_cream_PNG5091_03.jpg', 'extras'),
(4, 'Live Jalaibi', NULL, 50, 'repeated', 'jalebi_03.jpg', 'food'),
(5, 'Rabri Milk', NULL, 60, 'repeated', 'rabri-recipe_03.jpg', 'drinks'),
(6, 'khumariyaan', NULL, 10000, 'single', 'music.jpg', 'music');

-- --------------------------------------------------------

--
-- Table structure for table `hall`
--

CREATE TABLE IF NOT EXISTS `hall` (
  `hid` int(11) NOT NULL,
  `name` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `per_head_charge` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall`
--

INSERT INTO `hall` (`hid`, `name`, `location`, `min`, `max`, `per_head_charge`, `status`) VALUES
(1, 'Arena Ross Hall', 'arena', 200, 425, 1000, 1),
(2, 'Arena Jasmine Hall', 'arena', 200, 425, 800, 1),
(3, 'Arena Daisy Hall', 'arena', 200, 425, 1000, 1),
(4, 'Shiraz Inn', 'inn', 50, 500, 500, 1),
(5, 'Shiraz Ronaq', 'ronaq', 0, 0, NULL, 1),
(6, 'Shiraz Gathering', 'gathering', 600, 700, 1200, 1);

-- --------------------------------------------------------

--
-- Table structure for table `items_list`
--

CREATE TABLE IF NOT EXISTS `items_list` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `ingredients` longtext
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items_list`
--

INSERT INTO `items_list` (`id`, `title`, `ingredients`) VALUES
(1, 'Naranj Pulao', NULL),
(2, 'Chicken Roast	', NULL),
(3, 'Chicken Seekh Kabab', NULL),
(4, 'Fried Fish / Crispy Fried Fish', NULL),
(5, 'Mutton White Qorma', NULL),
(6, 'Chicken Ginger', NULL),
(7, 'Fruit Trifle ', NULL),
(8, 'Shahi Tukra', NULL),
(9, 'Salads (3)', NULL),
(10, 'Naan', NULL),
(11, 'Raita', NULL),
(12, 'Cold Drinks', NULL),
(13, 'Mineral Water', NULL),
(14, 'Beef Pulao', NULL),
(15, 'Chicken Roast', NULL),
(16, 'Seekh Kabab', NULL),
(17, 'Beef Saag', NULL),
(18, 'Kofta Curry / Beef Dal Qorma', NULL),
(19, 'Suji Halwa', NULL),
(20, 'Salads (2)', NULL),
(21, 'Ch-Achari / Ch- Saag ', NULL),
(22, 'Beef Dal Qorma', NULL),
(23, 'Kheer', NULL),
(24, 'Zarda / Suji Halwa', NULL),
(25, 'Beef Pulao / Naranj Pulao', NULL),
(26, 'Chicken Boti (B.B.Q)', NULL),
(27, 'Mutton Brown Qorma', NULL),
(28, 'Saag Paneer', NULL),
(29, 'Alu Bukhara Chutney', NULL),
(30, 'Salad (3)', NULL),
(31, 'Mutton Biryani / Beef Pulao', NULL),
(32, 'Mutton Roast', NULL),
(33, 'Fried Fish', NULL),
(34, 'Mutton Qorma/White Qorma', NULL),
(35, 'Chicken Ginger / Jelfraizi', NULL),
(36, 'Sweets (4)', NULL),
(37, 'Salads (4)', NULL),
(38, 'Channa Mewa / Beef Pulao', NULL),
(39, 'Chicken Curry', NULL),
(40, 'Channay', NULL),
(41, 'Halwa', NULL),
(42, 'Puri', NULL),
(43, 'Fresh Salad', NULL),
(44, 'Gol Gappy (Ladies Section)', NULL),
(45, 'Kabuli Pulao', NULL),
(46, 'Chalaw', NULL),
(47, 'kabab-e-Muragh ', NULL),
(48, 'Dashi Kabab', NULL),
(49, 'kofta korma', NULL),
(50, 'Nukal Semian', NULL),
(51, 'Seasonal Fruit 02', NULL),
(52, 'Ferni + Jelly', NULL),
(53, 'Salad', NULL),
(54, 'Green Tea', NULL),
(55, 'Narang Pulao', NULL),
(56, 'Chalaw Lydam', NULL),
(57, 'Kabab-e- Koh', NULL),
(58, 'Chips', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`mid` int(11) NOT NULL,
  `name` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `items` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`mid`, `name`, `image`, `items`, `cat_id`, `hall_id`, `price`, `status`) VALUES
(1, 'Shiraz Classic (New)', '11856773_669940243149658_1341035110_n.jpg', '14,15,16,17,18,19,20,10,11,12,13', 1, 0, 1080, 1),
(2, 'Shiraz Silver (New)', '11856627_419494058259811_118998485_n.jpg', '14,2,16,21,22,23,24,20,10,11,12,13', 1, 0, 1140, 1),
(3, 'Shiraz Gold (New)', '11373951_1628364544091299_1152401812_n.jpg', '25,26,16,27,28,23,19,29,30,10,11,12,13', 1, 0, 1260, 1),
(4, 'Shiraz Special (New)', '11809944_920536878011498_2040709310_n.jpg', '1,2,3,4,5,6,7,8,9,10,11,12,13', 1, 0, 1740, 1),
(5, 'Shiraz Platinum ', '11821901_1673421719548414_1020667022_n.jpg', '31,32,15,16,33,34,35,36,37,10,11,12,13', 1, 0, 2330, 1),
(6, 'Shiraz Nakreezay (New)', '11350675_1619036438351149_1148581680_n.jpg', '38,16,39,40,41,42,43,11,12,13,14,44', 1, 0, 970, 1),
(9, 'Shiraz Afghani No 1', '11349227_1482749762045885_1557439899_n.jpg', '45,46,47,48,49,50,51,52,53,10,54,12,13', 2, 0, 1050, 1),
(10, 'Shiraz Afghani Special', '11189791_963075673726725_139160886_n.jpg', '55,46,56,57,47,48,58,49,50,51,52,53,10,54,12,13', 2, 0, 1150, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` text,
  `contact` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'normal',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `address`, `contact`, `type`, `status`) VALUES
(1, 'waqas khan', 'waqas@hiwaas.com', '21232f297a57a5a743894a0e4a801fc3', 'Risalpur', '03018585160', 'admin', 1),
(2, 'Ali Khan', 'ali@example.com', '', 'Peshawar', '7218462172871', 'normal', 1),
(4, 'Khan Zaman', 'khan@example.com', '', 'Risalpur', '673409723', 'normal', 1),
(5, 'Javed Khan', 'javed@example.com', '', 'Nowshera', '758327583927', 'normal', 1),
(6, 'Alia bhatt', 'alia@example.com', '', 'Pakistan', '893750923852', 'normal', 1),
(7, 'Ehsan', 'ehsan@example.com', '', 'Mardan', '467281648912', 'normal', 1),
(8, 'Wajid Khan', 'wajid@example.com', '', 'Mardan', '24872164871', 'normal', 1),
(9, 'Uzair Khattak', 'uzair@example.com', '', 'Peshawar', '03219021814', 'normal', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`eid`);

--
-- Indexes for table `extra_items`
--
ALTER TABLE `extra_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items_list`
--
ALTER TABLE `items_list`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `extra_items`
--
ALTER TABLE `extra_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `items_list`
--
ALTER TABLE `items_list`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
